package com.example.seif.jahhizlyadmin2.Profile;

import android.content.Context;

import com.android.volley.VolleyError;
import com.example.seif.jahhizlyadmin2.Utils.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by seif on 1/8/2018.
 */
public class ProfilePresenter implements ProfileModel.VolleyCallback, ProfileMVP.presenter {
    ProfileModel model;
    ProfileMVP.view view;

    public ProfilePresenter(ProfileModel model, ProfileMVP.view view) {
        this.model = model;
        this.view = view;
    }

    @Override
    public void requestUserData(Context context) {
        view.showProgress();
        model.profile(context, this);
    }

    @Override
    public void onSuccess(Context context, String response) throws JSONException {
        view.hideProgress();
        JSONObject jsonObject = new JSONObject(response);
        JSONObject user = jsonObject.getJSONObject("user");
        JSONObject branch = jsonObject.getJSONObject("branch");
        JSONObject brand = jsonObject.getJSONObject("brand");
        int id = user.getInt("id");
        int adminId = user.getInt("admin_id");
        String email = user.getString("email");
        String phone = user.getString("phone");
        String userName = user.getString("name");
        String photo;
        if (user.getString("photo").equals("") ||
                user.getString("photo").equals("null")) {
            photo = "";
        } else {
            photo = user.getString("photo");
        }
        String birthday;
        if (user.getString("birthday").equals("") ||
                user.getString("birthday").equals("null")) {
            birthday = "";
        } else if (user.getString("birthday").contains("T")) {
            birthday = user.getString("birthday").split("T")[0];
        } else {
            birthday = user.getString("birthday");
        }
        // female 1... male 0
        String gender;
        if (user.getInt("gender") == 0) {
            gender = "Male";
        } else {
            gender = "Female";
        }

        int branchId = branch.getInt("id");
        String branchName = branch.getString("name");
        String branchArabicName = branch.getString("arabic_name");
        String branchPhone;
        if (branch.getString("phone").equals("") ||
                branch.getString("phone").equals("null")) {
            branchPhone = "";
        } else {
            branchPhone = branch.getString("phone");
        }
        String tablePositionPhoto;
        if (branch.getString("tables_positions").equals("") ||
                branch.getString("tables_positions").equals("null")) {
            tablePositionPhoto = "";
        } else {
            tablePositionPhoto = branch.getString("tables_positions");
        }

        int brandId = brand.getInt("id");
        String brandLogo;
        if (brand.getString("photo").equals("") ||
                brand.getString("photo").equals("null")) {
            brandLogo = "";
        } else {
            brandLogo = brand.getString("photo");
        }
        MySingleton.getmInstance(context).saveUserData(new UserInfo(id, adminId, email, phone, photo, userName, birthday,
                gender, branchId, branchName, branchArabicName, branchPhone, tablePositionPhoto, brandId, brandLogo));
        view.setUserData(new UserInfo(id, adminId, email, phone, photo, userName, birthday,
                gender, branchId, branchName, branchArabicName, branchPhone, tablePositionPhoto, brandId, brandLogo));
    }

    @Override
    public void onFail(Context context, VolleyError error) {
        view.hideProgress();
        view.setUserData(MySingleton.getmInstance(context).userData());
    }


}
