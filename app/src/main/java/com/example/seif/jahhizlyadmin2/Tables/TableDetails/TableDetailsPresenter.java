package com.example.seif.jahhizlyadmin2.Tables.TableDetails;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.example.seif.jahhizlyadmin2.R;
import com.example.seif.jahhizlyadmin2.Tables.ReservedTables;
import com.example.seif.jahhizlyadmin2.Tables.TablesModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by seif on 1/9/2018.
 */
public class TableDetailsPresenter implements TableDetailsMVP.presenter, TableDetailsModel.VolleyCallback {
    TableDetailsMVP.view view;
    TableDetailsModel model;

    public TableDetailsPresenter(TableDetailsMVP.view view, TableDetailsModel model) {
        this.view = view;
        this.model = model;
    }


    @Override
    public void onTableDetailsCreated(ReservedTables table) {
        view.setTableData(table);
    }

    @Override
    public void onConfirmBtnClicked(Context context, int reservationId, int userId, int branchId, String tablePlace, String guestType, String note, String chairsCount, String time, int orderId, int status) throws JSONException {
        view.showProgress();
        model.confirmReservation(context, this, reservationId, userId, branchId, tablePlace, guestType, note, chairsCount, time, orderId, status);
    }

    @Override
    public void onSuccess(Context context, JSONObject response, int which) throws JSONException {
        if (which == 2) {
            view.hideProgress();
            view.editionDone(context.getString(R.string.success));
        }
    }

    @Override
    public void onFail(Context context, VolleyError error, int which) {
        view.hideProgress();
        if (error instanceof NoConnectionError || error instanceof TimeoutError) {
            view.showMessage(context.getString(R.string.internet_error));
            return;
        } else if (error instanceof ServerError) {
            view.showMessage(context.getString(R.string.server_error));
            return;
        } else {
            view.showMessage(context.getString(R.string.wrong));
        }
    }

}
