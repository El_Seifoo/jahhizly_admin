package com.example.seif.jahhizlyadmin2.Profile;

import android.content.Context;

/**
 * Created by seif on 1/8/2018.
 */
public interface ProfileMVP {
    interface view{
        void setUserData(UserInfo userInfo);
        void showProgress();
        void hideProgress();
    }

    interface presenter{
        void requestUserData(Context context);
    }
}
