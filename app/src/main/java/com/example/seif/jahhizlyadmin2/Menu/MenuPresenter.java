package com.example.seif.jahhizlyadmin2.Menu;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails.ItemExtrasDetails;
import com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails.ItemSizeDetails;
import com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails.OrdersDetails;
import com.example.seif.jahhizlyadmin2.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by seif on 1/7/2018.
 */
public class MenuPresenter implements MenuMVP.presenter, MenuMVP.menuPresenter, MenuModel.VolleyCallback {
    private MenuModel model;
    private MenuMVP.view view;
    MenuMVP.menuView menuView;
    private boolean flag;

    public MenuPresenter(MenuModel model, MenuMVP.view view, boolean flag) {
        this.model = model;
        this.view = view;
        this.flag = flag;
    }

    public MenuPresenter(MenuModel model, MenuMVP.menuView menuView, boolean flag) {
        this.model = model;
        this.menuView = menuView;
        this.flag = flag;
    }

    @Override
    public void requestBrandCategories(Context context, ViewPager viewPager, int brandId) {
        view.showProgress();
        model.getBrandCategories(context, this, viewPager, brandId);
    }

    @Override
    public void requestBranchMenu(Context context, int branchId, int categoryId) {
        menuView.showProgress();
        model.getBranchMenu(context, this, branchId, categoryId);
    }

    private Dialog mainDialog;

    @Override
    public void onMenuListItemClicked(Dialog dialog, int menuId, int index) {
        mainDialog = dialog;
        menuView.createDialog(dialog, menuId, index);
    }

    @Override
    public void requestSizesExtras(Context context, Dialog dialog, int menuId, int index) {
        menuView.showDialogProgress(dialog);
        model.getSizesExtras(context, this, menuId, index);
    }

    @Override
    public void onAddItemClicked(Dialog mainDialog, Dialog dialog, ArrayList<OrdersDetails> addedItems) {
        menuView.showDialogOptions(mainDialog, dialog, addedItems);
    }

    @Override
    public void onPlusClicked() {
        menuView.increase();
    }

    @Override
    public void onMinusClicked() {
        menuView.decrease();
    }

    @Override
    public void onSuccess(Context context, ViewPager viewPager, String response, int which, int index) throws JSONException {
        Log.e("menu presenter", response);
        if (which == 1) {
            view.hideProgress();
            ArrayList<BrandCategories> categoriesList = new ArrayList<>();
            JSONObject jsonResponse = new JSONObject(response);
            JSONArray menuCategories = jsonResponse.getJSONArray("menuCategories");
            for (int i = 0; i < menuCategories.length(); i++) {
                int id = menuCategories.getJSONObject(i).getInt("id");
                String name;
                if (menuCategories.getJSONObject(i).getString("name").equals("")
                        || menuCategories.getJSONObject(i).getString("name").equals("null")) {
                    name = context.getString(R.string.empty_list);
                } else {
                    if (flag)
                        name = menuCategories.getJSONObject(i).getString("arabic_name");
                    else name = menuCategories.getJSONObject(i).getString("name");
                }
                categoriesList.add(new BrandCategories(id, name));
            }
            view.setupViewPager(viewPager, categoriesList);
        } else if (which == 2) {
            menuView.hideProgress();
            ArrayList<MenuItem> menuItemDetails = new ArrayList<>();
            JSONObject jsonResponse = new JSONObject(response);
            JSONArray menus = jsonResponse.getJSONArray("menus");
            int branchesLength = menus.length();
            for (int i = 0; i < branchesLength; i++) {
                int id = menus.getJSONObject(i).getInt("id");
                int branchId = menus.getJSONObject(i).getInt("branch_id");
                String name = menus.getJSONObject(i).getString("name");
                String arabicName;
                if (flag) arabicName = menus.getJSONObject(i).getString("arabic_name");
                else arabicName = menus.getJSONObject(i).getString("name");

                String recipe;
                if (flag) recipe = menus.getJSONObject(i).getString("arabic_recipe");
                else recipe = menus.getJSONObject(i).getString("recipe");
                String photo;
                if (menus.getJSONObject(i).getString("photo").equals("") ||
                        menus.getJSONObject(i).getString("photo").equals("null")) {
                    photo = "null";
                } else {
                    photo = menus.getJSONObject(i).getString("photo");
                }
                String time = menus.getJSONObject(i).getString("time");
                int statusInt = menus.getJSONObject(i).getInt("status");
                boolean isAvailable;
                if (statusInt == 1) {
                    isAvailable = true;
                } else {
                    isAvailable = false;
                }
                int menuCategoryId = menus.getJSONObject(i).getInt("menu_category_id");
                double price = menus.getJSONObject(i).getDouble("price");
                menuItemDetails.add(new MenuItem(id, branchId, name, arabicName, recipe,
                        photo, time, isAvailable, menuCategoryId, false, price));
            }
            if (menuItemDetails.size() > 0) {
                menuView.loadData(menuItemDetails);
            } else {
                menuView.showEmptyTxt();
            }
        } else if (which == 3) {
            ArrayList<ItemSizeDetails> itemSizeDetails = new ArrayList<>();
            ArrayList<ItemExtrasDetails> itemExtrasDetails = new ArrayList<>();
            JSONObject jsonResponse = new JSONObject(response);
            JSONArray sizes = jsonResponse.getJSONArray("sizes");
            JSONArray extras = jsonResponse.getJSONArray("extras");
            for (int i = 0; i < sizes.length(); i++) {
                int id = sizes.getJSONObject(i).getInt("id");
                String size;
                if (flag) size = sizes.getJSONObject(i).getString("arabic_size");
                else size = sizes.getJSONObject(i).getString("size");
                double price = sizes.getJSONObject(i).getDouble("price");
                itemSizeDetails.add(new ItemSizeDetails(id, size, price));
            }
            for (int i = 0; i < extras.length(); i++) {
                int id = extras.getJSONObject(i).getInt("id");
                String name;
                if (flag) name = extras.getJSONObject(i).getString("arabic_name");
                else name = extras.getJSONObject(i).getString("name");
                double price = extras.getJSONObject(i).getDouble("price");
                itemExtrasDetails.add(new ItemExtrasDetails(id, name, price));
            }
            menuView.hideDialogProgress(mainDialog);
            menuView.sizesList(mainDialog, itemSizeDetails, index);
            menuView.checkBoxExtra(mainDialog, itemExtrasDetails, index);
        }
    }

    @Override
    public void onFail(Context context, VolleyError error, int which) {
        if (which == 1) {
            view.hideProgress();
        } else if (which == 2) {
            menuView.hideProgress();
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
            } else if (error instanceof ServerError) {
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
            }
        } else if (which == 3) {
            menuView.hideDialogProgress(mainDialog);
            mainDialog.dismiss();
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                menuView.showMessage(context.getString(R.string.internet_error));
            } else if (error instanceof ServerError) {
                menuView.showMessage(context.getString(R.string.server_error));
            } else {
                menuView.showMessage(context.getString(R.string.wrong));
            }
        }
    }

}
