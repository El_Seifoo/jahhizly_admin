package com.example.seif.jahhizlyadmin2.Menu;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.view.ViewPager;

import com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails.ItemExtrasDetails;
import com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails.ItemSizeDetails;
import com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails.OrdersDetails;

import java.util.ArrayList;

/**
 * Created by seif on 1/7/2018.
 */
public interface MenuMVP {
    interface view {
        void showProgress();

        void hideProgress();

        void setupViewPager(ViewPager viewPager, ArrayList<BrandCategories> categories);

    }

    interface presenter {
        void requestBrandCategories(Context context, ViewPager viewPager, int BrandId);

    }

    interface menuView {
        void showProgress();

        void hideProgress();

        void loadData(ArrayList<MenuItem> menus);

        void showMessage(String message);

        void showEmptyTxt();

        void createDialog(Dialog dialog, int menuId, int index);

        void showDialogProgress(Dialog dialog);

        void hideDialogProgress(Dialog dialog);

        void sizesList(Dialog dialog, ArrayList<ItemSizeDetails> itemSizeDetails, int index);

        void checkBoxExtra(Dialog dialog, ArrayList<ItemExtrasDetails> itemExtrasDetails, int index);

        void increase();

        void decrease();

        int getQuantity();

        int getSizeId();

        String getSize();

        String getExtrasId();

        String getExtras();

        double getPrice();

        void showDialogOptions(Dialog mainDialog, Dialog dialog, ArrayList<OrdersDetails> addedItems);

    }

    interface menuPresenter {
        void requestBranchMenu(Context context, int branchId, int categoryId);

        void onMenuListItemClicked(Dialog dialog, int menuId, int index);

        void requestSizesExtras(Context context, Dialog dialog, int menuId, int index);

        void onAddItemClicked(Dialog mainDialog, Dialog dialog, ArrayList<OrdersDetails> addedItems);

        void onPlusClicked();

        void onMinusClicked();

    }
}
