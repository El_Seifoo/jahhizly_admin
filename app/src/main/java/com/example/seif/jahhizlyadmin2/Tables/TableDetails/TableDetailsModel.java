package com.example.seif.jahhizlyadmin2.Tables.TableDetails;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.seif.jahhizlyadmin2.Utils.MySingleton;
import com.example.seif.jahhizlyadmin2.Utils.URLs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by seif on 1/9/2018.
 */
public class TableDetailsModel {
    public TableDetailsModel() {
    }

    protected void confirmReservation(final Context context, final VolleyCallback callback, int reservationId, int userId, int branchId, String tablePlace, String guestType, String note, String chairsCount, String time, int orderId, int status) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", reservationId);
        jsonObject.put("user_id", userId);
        jsonObject.put("branch_id", branchId);
        jsonObject.put("chairs_count", chairsCount);
        jsonObject.put("table_place", tablePlace);
        jsonObject.put("guest_type", guestType);
        jsonObject.put("reservation_time", time);
        jsonObject.put("status", status);
        if (orderId != -1) {
            jsonObject.put("order_id", orderId);
        }
        jsonObject.put("note", note);

        Log.e("json", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                URLs.EDIT_RESERVATION,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Edit Reserv response", response.toString());
                        try {
                            callback.onSuccess(context, response, 2);
                        } catch (JSONException e) {
                            Log.e("Edit Parse response", e.toString());
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Edit Reserv error", error.toString());
                        callback.onFail(context, error, 2);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(jsonObjectRequest);

    }


    protected interface VolleyCallback {
        void onSuccess(Context context, JSONObject response, int which) throws JSONException;

        void onFail(Context context, VolleyError error, int which);
    }
}
