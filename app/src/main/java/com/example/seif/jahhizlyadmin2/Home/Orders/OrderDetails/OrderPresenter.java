package com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.example.seif.jahhizlyadmin2.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by seif on 1/4/2018.
 */
public class OrderPresenter implements OrderMVP.presenter, OrderModel.VolleyCallback {
    private OrderModel model;
    private OrderMVP.view view;
    private boolean flag;

    public OrderPresenter(OrderModel model, OrderMVP.view view, boolean flag) {
        this.model = model;
        this.view = view;
        this.flag = flag;
    }


    @Override
    public void requestOrderDetails(Context context, int orderId) {
        view.showProgress();
        model.getOrderDetails(context, this, orderId);
    }

    Dialog mainDialog;

    @Override
    public void onListItemClicked(Context context, Dialog dialog, int menuId, int index) {
        mainDialog = dialog;
        view.showItemDialog(dialog, menuId, index);
    }

    @Override
    public void requestSizesExtras(Context context, Dialog dialog, int menuId, int index) {
        view.showDialogProgress();
        model.getSizesExtras(context, this, menuId, index);
    }

    @Override
    public void onPlusClicked() {
        view.increase();
    }

    @Override
    public void onMinusClicked() {
        view.decrease();
    }

    @Override
    public void onEditBtnClicked(Dialog dialog, ArrayList<OrdersDetails> list, int index, int sizeId, String size, String extrasId, String extras, int quantity, double price, double extrasPrice) {
        view.editListItem(dialog, list, index, sizeId, size, extrasId, extras, quantity, price, extrasPrice);
    }

    @Override
    public void saveEditedData(Context context, int orderId, int userId, int branchId, int orderTime, ArrayList<OrdersDetails> orderList, int status) {
        view.showProgress();
        try {
            model.editOrder(context, this, orderId, userId, branchId, orderTime, orderList, status);
        } catch (JSONException e) {
            Log.e("Edit Order Parsing JSON", e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public void deleteOrderClicked(Context context, int orderId) {
        view.showProgress();
        model.deleteOrder(context, this, orderId);
    }

    @Override
    public void onSuccess(Context context, String response, int which, int index) throws JSONException {
        if (which == 1) {
            Log.e("Order Details response", response);
            view.hideProgress();
            ArrayList<OrdersDetails> order = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(response);
            JSONArray orders = jsonObject.getJSONArray("orders");
            for (int i = 0; i < orders.length(); i++) {
                int id = orders.getJSONObject(i).getInt("id");
                int orderId = orders.getJSONObject(i).getInt("order_id");
                int menuId = orders.getJSONObject(i).getInt("menu_id");
                int brandId = orders.getJSONObject(i).getInt("brand_id");
                int quantity = orders.getJSONObject(i).getInt("quantity");
                double price = orders.getJSONObject(i).getDouble("price");
                String itemName;
                if (flag)
                    itemName = orders.getJSONObject(i).getString("item_arabic_name");
                else itemName = orders.getJSONObject(i).getString("item_name");
                Log.e("itemName", itemName);
                String extraId = orders.getJSONObject(i).getString("extra_id");
                String extraName;
                if (orders.getJSONObject(i).getString("extra_name").equals("") ||
                        orders.getJSONObject(i).getString("extra_name").equals("null")) {
                    extraName = "";
                } else {
                    if (flag)
                        extraName = orders.getJSONObject(i).getString("extra_arabic_name");
                    else extraName = orders.getJSONObject(i).getString("extra_name");
                }
                String extraPrice;
                if (orders.getJSONObject(i).getString("extra_price").equals("") ||
                        orders.getJSONObject(i).getString("extra_price").equals("null")) {
                    extraPrice = "";
                } else {
                    extraPrice = orders.getJSONObject(i).getString("extra_price");
                }
                int sizeId = orders.getJSONObject(i).getInt("size_id");
                String size;
                if (flag) size = orders.getJSONObject(i).getString("arabic_size");
                else size = orders.getJSONObject(i).getString("size");
                String photo;
                if (orders.getJSONObject(i).getString("item_photo").equals("") ||
                        orders.getJSONObject(i).getString("item_photo").equals("null")) {
                    photo = "null";
                } else {
                    photo = orders.getJSONObject(i).getString("item_photo");
                }
                order.add(new OrdersDetails(quantity, price, itemName, extraId,
                        extraName, extraPrice, sizeId, size, id, orderId, menuId, brandId, photo));
            }
            if (order.size() > 0) {
                arrangeData(order);
            } else {
                view.showEmptyText();
            }
        } else if (which == 2) {
            Log.e("orderPresenter 2", response);
            view.hideDialogProgress();
            ArrayList<ItemSizeDetails> itemSizeDetails = new ArrayList<>();
            ArrayList<ItemExtrasDetails> itemExtrasDetails = new ArrayList<>();
            JSONObject jsonResponse = new JSONObject(response);
            JSONArray sizes = jsonResponse.getJSONArray("sizes");
            JSONArray extras = jsonResponse.getJSONArray("extras");
            for (int i = 0; i < sizes.length(); i++) {
                int id = sizes.getJSONObject(i).getInt("id");
                String size;
                if (flag) size = sizes.getJSONObject(i).getString("arabic_size");
                else size = sizes.getJSONObject(i).getString("size");
                double price = sizes.getJSONObject(i).getDouble("price");
                itemSizeDetails.add(new ItemSizeDetails(id, size, price));
            }
            for (int i = 0; i < extras.length(); i++) {
                int id = extras.getJSONObject(i).getInt("id");
                String name;
                if (flag) name = extras.getJSONObject(i).getString("arabic_name");
                else name = extras.getJSONObject(i).getString("name");
                double price = extras.getJSONObject(i).getDouble("price");
                itemExtrasDetails.add(new ItemExtrasDetails(id, name, price));
            }
            view.hideDialogProgress();
            view.sizesList(mainDialog, itemSizeDetails, index);
            view.checkBoxExtra(mainDialog, itemExtrasDetails, index);
        } else if (which == 3) {
            view.hideProgress();
            view.deletionDone(context.getString(R.string.success));
        } else if (which == 4) {
            view.hideProgress();
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equals("done")) {
                view.deletionDone(context.getString(R.string.success));
            } else {
                view.showMessage(context.getString(R.string.wrong));
            }
        }
    }

    private void arrangeData(ArrayList<OrdersDetails> orders) {
        int i = 0;
        int x = 1;
        if (orders.size() > 1) {
            while (i < orders.size()) {
                if (orders.get(i).getId() == orders.get(x).getId() &&
                        orders.get(i).getMenuId() == orders.get(x).getMenuId()) {
                    orders.get(i).setExtraName(orders.get(i).getExtraName() + "," + orders.get(x).getExtraName());
                    orders.get(i).setExtraPrice(orders.get(i).getExtraPrice() + "," + orders.get(x).getExtraPrice());
                    orders.get(i).setExtraId(orders.get(i).getExtraId() + "," + orders.get(x).getExtraId());
                    orders.remove(x);
                } else {
                    x++;
                }

                if (x == orders.size()) {
                    i++;
                    x = i + 1;
                    if (x == orders.size()) {
                        break;
                    }
                }
            }
        }

        view.loadData(orders);
    }


    @Override
    public void onFail(Context context, VolleyError error, int which) {
        if (which == 1) {
            view.hideProgress();
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                view.showMessage(context.getString(R.string.internet_error));
            } else if (error instanceof ServerError) {
                view.showMessage(context.getString(R.string.server_error));
            } else {
                view.showMessage(context.getString(R.string.wrong));
            }
        } else if (which == 2) {
            view.hideDialogProgress();
            mainDialog.dismiss();
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                view.showMessage(context.getString(R.string.internet_error));
            } else if (error instanceof ServerError) {
                view.showMessage(context.getString(R.string.server_error));
            } else {
                view.showMessage(context.getString(R.string.wrong));
            }
        } else if (which == 3) {
            view.hideProgress();
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                view.showMessage(context.getString(R.string.internet_error));
            } else if (error instanceof ServerError) {
                view.showMessage(context.getString(R.string.server_error));
            } else {
                view.showMessage(context.getString(R.string.wrong));
            }
        } else {
            view.hideProgress();
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                view.showMessage(context.getString(R.string.internet_error));
            } else if (error instanceof ServerError) {
                view.showMessage(context.getString(R.string.server_error));
            } else {
                view.showMessage(context.getString(R.string.wrong));
            }
        }

    }
}
