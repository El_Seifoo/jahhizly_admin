package com.example.seif.jahhizlyadmin2.Menu;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.seif.jahhizlyadmin2.Utils.MySingleton;
import com.example.seif.jahhizlyadmin2.Utils.URLs;

import org.json.JSONException;

/**
 * Created by seif on 1/7/2018.
 */
public class MenuModel {
    public MenuModel() {
    }

    protected void getBrandCategories(final Context context, final VolleyCallback callback, final ViewPager viewPager, int brandId) {
        String url = URLs.MENU_CATEGORIES + "/" + brandId;
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            callback.onSuccess(context, viewPager, response, 1, 0);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onFail(context, error, 1);
                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void getBranchMenu(final Context context, final VolleyCallback callback, int branchId, int categoryId) {
        String url = (categoryId == 0) ? URLs.BRANCH_MENU + "/" + branchId : URLs.BRANCH_MENU + "/" + branchId + "/" + categoryId;
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("branch menu response", response);
                        try {
                            callback.onSuccess(context, null, response, 2, 0);
                        } catch (JSONException e) {
                            Log.e("branch menu parsing", e.toString());
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("branch menu error", error.toString());
                        callback.onFail(context, error, 2);
                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(stringRequest);

    }

    protected void getSizesExtras(final Context context, final VolleyCallback callback, final int menuId, final int index) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URLs.MENU_ITEM_DETAILS + "/" + menuId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("menu item response", response);
                        try {
                            callback.onSuccess(context, null, response, 3, index);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("menu item parsing", e.toString());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("menu item error", error.toString());
                        callback.onFail(context, error, 2);
                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }


    protected interface VolleyCallback {
        void onSuccess(Context context, ViewPager viewPager, String response, int which, int index) throws JSONException;

        void onFail(Context context, VolleyError error, int which);
    }
}
