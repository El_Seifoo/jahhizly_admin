package com.example.seif.jahhizlyadmin2.Tables;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by seif on 1/8/2018.
 */
public interface TablesMVP {
    interface view {
        void showProgress();


        void hideProgress();

        void showLoadingMoreProgress();

        void hideLoadingMoreProgress();

        void showMessage(String message);

        void showEmptyTxt();

        void loadData(ArrayList<ReservedTables> tables);
    }

    interface presenter {
        void onTablesCreated(Context context, int page, String status, String key);

        void onTablesCreated(Context context, String status, String key);
    }
}
