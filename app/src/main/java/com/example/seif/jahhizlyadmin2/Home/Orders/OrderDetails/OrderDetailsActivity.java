package com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.LayoutDirection;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.seif.jahhizlyadmin2.Home.MainActivity;
import com.example.seif.jahhizlyadmin2.Home.Orders.Orders;
import com.example.seif.jahhizlyadmin2.Home.Orders.OrdersAdapter;
import com.example.seif.jahhizlyadmin2.Menu.MenuActivity;
import com.example.seif.jahhizlyadmin2.R;
import com.example.seif.jahhizlyadmin2.Utils.MySingleton;
import com.example.seif.jahhizlyadmin2.Utils.URLs;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import me.anwarshahriar.calligrapher.Calligrapher;

public class OrderDetailsActivity extends AppCompatActivity implements OrderMVP.view, OrderDetailsAdapter.OrderDetailsListItemClickListener,
        SizesAdapter.SizesListItemClickListener {
    private MKLoader loading, dialogLoading;
    private OrderPresenter presenter;
    private RecyclerView recyclerView;
    private OrderDetailsAdapter adapter;
    private ArrayList<OrdersDetails> allOrders;
    private final int ADD_ITEM_CODE = 1010;
    private int status;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        Calligrapher calligrapher = new Calligrapher(this);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(this, "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(this, "fonts/English/Roboto_Regular.ttf", true);
        }
        getSupportActionBar().setTitle(getString(R.string.prev_order_details_activity));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter = new OrderPresenter(new OrderModel(), this,
                MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
        loading = (MKLoader) findViewById(R.id.loading);
        recyclerView = (RecyclerView) findViewById(R.id.order_details_rec_view);
        adapter = new OrderDetailsAdapter(this, MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
        recyclerView.setHasFixedSize(true);
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                recyclerView.setLayoutManager(new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false));
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                recyclerView.setLayoutManager(new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false));
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                break;
            default:
                recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        }

        String statusString = ((Orders) getIntent().getSerializableExtra("UserOrder")).getStatus();
        status = (statusString.equals(getString(R.string.status_0))) ? 0 : (statusString.equals(getString(R.string.status_1))) ? 1 : (statusString.equals(getString(R.string.status_2))) ? 2 : (statusString.equals(getString(R.string.status_3))) ? 3 : 4;
        presenter.requestOrderDetails(this, ((Orders) getIntent().getSerializableExtra("UserOrder")).getId());
        ((Button) findViewById(R.id.save_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                double price = 0;
                for (int i = 0; i < allOrders.size(); i++) {
                    Log.e("ssssss", allOrders.get(i).toString());
                    price += ((allOrders.get(i).getPrice()) * allOrders.get(i).getQuantity());
                }
                Log.e("pricessssss", price + "");
                presenter.saveEditedData(OrderDetailsActivity.this, ((Orders) getIntent().getSerializableExtra("UserOrder")).getId(), ((Orders) getIntent().getSerializableExtra("UserOrder")).getUserId(),
                        ((Orders) getIntent().getSerializableExtra("UserOrder")).getBranchId(), ((Orders) getIntent().getSerializableExtra("UserOrder")).getPreparationTime(), allOrders, status);
            }
        });
        ((Button) findViewById(R.id.delete_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("id", ((Orders) getIntent().getSerializableExtra("UserOrder")).getId() + "");
                presenter.deleteOrderClicked(OrderDetailsActivity.this, ((Orders) getIntent().getSerializableExtra("UserOrder")).getId());
            }
        });

        ((Button) findViewById(R.id.add_item_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (allOrders != null && allOrders.size() > 0) {
                    for (int i = 0; i < allOrders.size(); i++) {
                        Log.e("orders size 3", allOrders.size() + "");
                    }
                    // add item function .....
                    Intent intent = new Intent(OrderDetailsActivity.this, MenuActivity.class);
                    //must be brandID not id (w8 taha till do it )
                    intent.putExtra("BrandID", allOrders.get(0).getBrandId());
                    intent.putExtra("BranchID", ((Orders) getIntent().getSerializableExtra("UserOrder")).getBranchId());
                    startActivityForResult(intent, ADD_ITEM_CODE);
                }

            }
        });

        // spinner status ....
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        final String[] statusArr = {getString(R.string.status_0), getString(R.string.status_1), getString(R.string.status_2), getString(R.string.status_3), getString(R.string.status_4)};
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item,
                        statusArr); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setSelection(status);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                ((Button) findViewById(R.id.save_btn)).setClickable(true);
                ((Button) findViewById(R.id.save_btn)).setEnabled(true);
                status = (statusArr[position].equals(getString(R.string.status_0))) ? 0 : (statusArr[position].equals(getString(R.string.status_1))) ? 1 : (statusArr[position].equals(getString(R.string.status_2))) ? 2 : (statusArr[position].equals(getString(R.string.status_3))) ? 3 : 4;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_ITEM_CODE && resultCode == RESULT_OK && data != null) {
            ArrayList<OrdersDetails> result = (ArrayList<OrdersDetails>) data.getSerializableExtra("result");
            for (OrdersDetails ordersDetails : result) {
                Log.e("orderDetailsActivity 1", ordersDetails.getExtraName());
            }
            for (OrdersDetails ordersDetails : allOrders) {
                Log.e("orderDetailsActivity 2", ordersDetails.getExtraName());
            }

            for (int i = 0; i < result.size(); i++) {
                allOrders.add(result.get(i));
            }

            for (OrdersDetails ordersDetails : allOrders) {
                Log.e("orderDetailsActivity 3", ordersDetails.getExtraName());
            }
            ((Button) findViewById(R.id.save_btn)).setClickable(true);
            ((Button) findViewById(R.id.save_btn)).setEnabled(true);
            loadData(allOrders);
        }
    }

    @Override
    public void loadData(ArrayList<OrdersDetails> orders) {
        for (int i = 0; i < orders.size(); i++) {
            Log.e("orders size 1", orders.size() + "");
        }
        allOrders = orders;
        for (int i = 0; i < allOrders.size(); i++) {
            Log.e("orders size 2", allOrders.size() + "");
        }
        adapter.setOrdersDetails(orders);
        ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
        scaleInAnimationAdapter.setFirstOnly(false);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showProgress() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        loading.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyText() {
        ((TextView) findViewById(R.id.empty_list_text)).setVisibility(View.VISIBLE);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }


    @Override
    public void onListItemClickListener(int position) {
        presenter.onListItemClicked(this, new Dialog(this), allOrders.get(position).getMenuId(), position);
    }

    TextView counter, cartDialogPrice;
    int counterInteger;


    @Override
    public void showItemDialog(final Dialog dialog, int menuId, final int index) {
        dialog.setContentView(R.layout.menu_item_dialoge);
        dialogLoading = (MKLoader) dialog.findViewById(R.id.dialog_loading);
        presenter.requestSizesExtras(this, dialog, menuId, index);
        ImageView increase = (ImageView) dialog.findViewById(R.id.cart_dialog_increase);
        ImageView decrease = (ImageView) dialog.findViewById(R.id.cart_dialog_decrease);
        counter = (TextView) dialog.findViewById(R.id.cart_dialog_counter);
        counterInteger = allOrders.get(index).getQuantity();
        counter.setText(MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(allOrders.get(index).getQuantity() + "") : allOrders.get(index).getQuantity() + "");
        final TextView cartDialogName = (TextView) dialog.findViewById(R.id.menu_item_name);
        cartDialogPrice = (TextView) dialog.findViewById(R.id.cart_dialog_price);
        Log.e("testPrice", allOrders.get(index).getPrice() + "");
        String priceTxt = getString(R.string.price) +
                "<font color = '#65db73'>" + String.valueOf((extraPrice * allOrders.get(index).getQuantity()) + (allOrders.get(index).getPrice() * allOrders.get(index).getQuantity())) + getString(R.string.currency) + "</font>";
        cartDialogPrice.setText(Html.fromHtml(MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(priceTxt) : priceTxt), TextView.BufferType.SPANNABLE);
        CircleImageView cartDialogImg = (CircleImageView) dialog.findViewById(R.id.menu_item_img);
        cartDialogName.setText(MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(" " + allOrders.get(index).getItemName() + " ") : " " + allOrders.get(index).getItemName() + " ");
        Glide.with(this).load(URLs.ROOT_URL + allOrders.get(index).getPhoto())
                .error(R.mipmap.restaurant_meal_icon)
                .thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(cartDialogImg);
        increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onPlusClicked();


            }
        });

        decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onMinusClicked();

            }
        });

        Button editBtn = (Button) dialog.findViewById(R.id.edit_btn);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                presenter.onEditBtnClicked(dialog, allOrders, index, getSizeId(), getSize(), getExtrasId(), getExtras(), getQuantity(), getPrice(), getExtrasPrice());
                dialog.dismiss();
            }
        });
        Button cancel = (Button) dialog.findViewById(R.id.dialog_cancel_btn);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (sizes != null && sizes.size() > 0) {
                    sizes.clear();
                }
                if (prices != null && prices.size() > 0) {
                    prices.clear();
                }
                if (extras != null && extras.size() > 0) {
                    extras.clear();
                }
                extraPrice = 0;
                size = "";
                price = 0;
                counterInteger = 0;
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                Toast.makeText(this, "1", Toast.LENGTH_SHORT).show();
                dialog.getWindow().setLayout(2 * (metrics.widthPixels) / 3, LinearLayout.LayoutParams.WRAP_CONTENT);
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                dialog.getWindow().setLayout(2 * (metrics.widthPixels) / 2, LinearLayout.LayoutParams.WRAP_CONTENT);
                break;
            default:
                dialog.getWindow().setLayout((metrics.widthPixels), LinearLayout.LayoutParams.WRAP_CONTENT);
        }
        dialog.show();
    }

    @Override
    public void increase() {
        if (counterInteger >= 99) {
            counterInteger = 99;
        } else {
            counterInteger++;
        }
        counter.setText(MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(String.valueOf(counterInteger)) : String.valueOf(counterInteger));
        String priceTxt = getString(R.string.price) +
                "<font color = '#65db73'>" + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency) + "</font>";
        cartDialogPrice.setText(Html.fromHtml(MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(priceTxt) : priceTxt), TextView.BufferType.SPANNABLE);
    }

    @Override
    public void decrease() {
        if (counterInteger <= 1) {
            counterInteger = 1;
        } else {
            counterInteger--;
        }
        counter.setText(MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(String.valueOf(counterInteger)) : String.valueOf(counterInteger));
        String priceTxt = getString(R.string.price) +
                "<font color = '#65db73'>" + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency) + "</font>";
        cartDialogPrice.setText(Html.fromHtml(MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(priceTxt) : priceTxt), TextView.BufferType.SPANNABLE);
    }

    ArrayList<String> sizes = new ArrayList<>();
    ArrayList<Double> prices = new ArrayList<>();
    ArrayList<Integer> sizesIds = new ArrayList<>();
    private String size;
    private double price = 0;
    private int sizeId;
    SizesAdapter sizesAdapter;

    @Override
    public void onSizeListItemClicked(int position) {
        size = sizes.get(position);
        price = prices.get(position);
        sizeId = sizesIds.get(position);
        String priceTxt = getString(R.string.price) +
                "<font color = '#65db73'>" + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency) + "</font>";
        cartDialogPrice.setText(Html.fromHtml(MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(priceTxt) : priceTxt), TextView.BufferType.SPANNABLE);
    }

    @Override
    public void sizesList(Dialog dialog, ArrayList<ItemSizeDetails> itemSizeDetails, int index) {
        size = allOrders.get(index).getSize();
        price = allOrders.get(index).getPrice();
        int position = 0;
        for (int i = 0; i < itemSizeDetails.size(); i++) {
            if (itemSizeDetails.get(i).getSize().equals(size)) {
                sizeId = itemSizeDetails.get(i).getId();
                position = i;
            }
            sizes.add(itemSizeDetails.get(i).getSize());
            prices.add(itemSizeDetails.get(i).getPrice());
            sizesIds.add(itemSizeDetails.get(i).getId());
        }
        String priceTxt = getString(R.string.price) +
                "<font color = '#65db73'>" + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency) + "</font>";
        cartDialogPrice.setText(Html.fromHtml(MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(priceTxt) : priceTxt), TextView.BufferType.SPANNABLE);
        itemSizeDetails.clear();
        RecyclerView rv = (RecyclerView) dialog.findViewById(R.id.sizes_rec_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv.setLayoutManager(layoutManager);
        rv.setHasFixedSize(true);
        sizesAdapter = new SizesAdapter(this, sizes, prices, this, MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
        sizesAdapter.setSelectedPosition(position);
        rv.setAdapter(sizesAdapter);
    }

    double extraPrice = 0;
    ArrayList<ItemExtrasDetails> extras;
    LinearLayout container;

    @Override
    public void checkBoxExtra(Dialog dialog, ArrayList<ItemExtrasDetails> itemExtrasDetails, int index) {
        extras = itemExtrasDetails;
        container = (LinearLayout) dialog.findViewById(R.id.extras_container);
        for (int i = 0; i < extras.size(); i++) {
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            final CheckBox checkBox = new CheckBox(new ContextThemeWrapper(this, R.style.customRadioBtn));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            checkBox.setLayoutParams(params);
            checkBox.setTag(extras.get(i).getPrice());
            checkBox.setText(MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(extras.get(i).getName()) : extras.get(i).getName());
            if (getResources().getConfiguration().getLayoutDirection() == LayoutDirection.RTL) {
                checkBox.setGravity(Gravity.RIGHT | Gravity.CENTER);
            }
            checkBox.setTextSize(15);
            checkBox.setChecked(false);
            checkBox.setTextColor(getResources().getColor(R.color.grey_5));
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (isChecked) {
                        extraPrice += Double.parseDouble(String.valueOf(compoundButton.getTag()));
                        String priceTxt = getString(R.string.price) +
                                "<font color = '#65db73'>" + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency) + "</font>";
                        cartDialogPrice.setText(Html.fromHtml(MySingleton.getmInstance(OrderDetailsActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? priceTxt : priceTxt), TextView.BufferType.SPANNABLE);


                    } else {
                        extraPrice -= Double.parseDouble(String.valueOf(compoundButton.getTag()));
                        String priceTxt = getString(R.string.price) +
                                "<font color = '#65db73'>" + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency) + "</font>";
                        cartDialogPrice.setText(Html.fromHtml(MySingleton.getmInstance(OrderDetailsActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(priceTxt) : priceTxt), TextView.BufferType.SPANNABLE);
                    }
                }
            });
            linearLayout.addView(checkBox, 0);

            TextView txt = new TextView(this);
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            checkBox.setLayoutParams(params1);
            txt.setText(MySingleton.getmInstance(OrderDetailsActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(String.valueOf(extras.get(i).getPrice()) + getString(R.string.currency)) : String.valueOf(extras.get(i).getPrice()) + getString(R.string.currency));
            txt.setTextColor(getResources().getColor(R.color.green_1));
            txt.setTextSize(15);
            txt.setGravity(Gravity.START);
            linearLayout.addView(txt, 1);
            container.addView(linearLayout);
        }


        // check the selected checkBoxes (depending on orderItem)
        ArrayList<String> allExtras = new ArrayList<>();
        if (allOrders.get(index).getExtraName().contains(",")) {
            String[] extras = allOrders.get(index).getExtraName().split(",");
            for (int i = 0; i < extras.length; i++) {
                allExtras.add(extras[i]);
            }

        } else {
            if (!allOrders.get(index).getExtraName().trim().equals("")) {
                allExtras.add(allOrders.get(index).getExtraName());
            }
        }
        if (!allExtras.isEmpty()) {
            for (int i = 0; i < container.getChildCount(); i++) {
                for (int y = 0; y < allExtras.size(); y++) {
                    if (itemExtrasDetails.get(i).getName().equals(allExtras.get(y))) {
                        ((CheckBox) ((LinearLayout) container.getChildAt(i)).getChildAt(0)).setChecked(true);
                    }

                }
            }
        }
    }

    @Override
    public void editListItem(Dialog dialog, ArrayList<OrdersDetails> list, int index, int sizeId, String size, String extrasId, String extras, int quantity, double price, double extrasPrices) {

        list.get(index).setSizeId(sizeId);
        list.get(index).setSize(size);
        list.get(index).setExtraId(extrasId);
        list.get(index).setExtraName(extras);
        list.get(index).setExtraPrice(String.valueOf(extrasPrices));
        list.get(index).setQuantity(quantity);
        list.get(index).setPrice(price);
        loadData(list);
        ((Button) findViewById(R.id.save_btn)).setClickable(true);
        ((Button) findViewById(R.id.save_btn)).setEnabled(true);
        Toast.makeText(this, getString(R.string.click_save_message), Toast.LENGTH_LONG).show();
    }

    @Override
    public int getSizeId() {
        return sizeId;
    }

    @Override
    public String getSize() {
        return size;
    }


    @Override
    public String getExtrasId() {
        String extrasId = "";
        double extrasPrice = 0;
        ArrayList<String> extrasData = getExtrasData();
        for (int i = 0; i < extras.size(); i++) {
            for (int y = 0; y < extrasData.size(); y++) {
                if (extras.get(i).getName().equals(extrasData.get(y))) {
                    extrasId += extras.get(i).getId();
                    extrasPrice += extras.get(i).getPrice();
                    if (y != extras.size() - 1) {
                        extrasId += ",";
                    }
                }
            }
        }
        return extrasId;
    }

    public double getExtrasPrice() {
        double totalExtrasPrice = 0;
        if (getExtrasId().contains(",")) {
            String[] extrasss = getExtrasId().split(",");
            for (int i = 0; i < extrasss.length; i++) {
                for (int j = 0; j < extras.size(); j++) {
                    if (extras.get(j).getId() == Double.valueOf(extrasss[0])) {
                        totalExtrasPrice += extras.get(j).getPrice();
                    }
                }
            }
        }
        return totalExtrasPrice;
    }

    @Override
    public String getExtras() {
        String extrasNames = "";
        if (getExtrasData() != null && getExtrasData().size() > 0) {
            ArrayList<String> extras = getExtrasData();
            for (int i = 0; i < extras.size(); i++) {
                extrasNames += extras.get(i);
                if (i != extras.size() - 1) {
                    extrasNames += ",";
                }
            }
        }
        return extrasNames;
    }


    @Override
    public double getPrice() {
//        double price1 = price + extraPrice;
//        double totalPrice = price1 * getQuantity();
        return price;
    }

    @Override
    public int getQuantity() {
        return counterInteger;
    }

    @Override
    public void deletionDone(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private ArrayList<String> getExtrasData() {
        ArrayList<String> checkBoxData = new ArrayList<>();
        int extraSize = container.getChildCount();
        for (int i = 0; i < extraSize; i++) {
            CheckBox extraCheckBox = (CheckBox) ((LinearLayout) container.getChildAt(i)).getChildAt(0);
            if (extraCheckBox.isChecked()) {
                checkBoxData.add(extraCheckBox.getText().toString().trim());
            }
        }
        return checkBoxData;
    }

    @Override
    public void showDialogProgress() {
        dialogLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideDialogProgress() {
        dialogLoading.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void deleteOrder() {
        presenter.deleteOrderClicked(OrderDetailsActivity.this, ((Orders) getIntent().getSerializableExtra("UserOrder")).getId());
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}
