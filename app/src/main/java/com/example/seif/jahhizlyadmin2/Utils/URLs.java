package com.example.seif.jahhizlyadmin2.Utils;

/**
 * Created by seif on 8/13/2017.
 */
public class URLs {
    public static final String ROOT_URL = "http://jahezzli.com/api/";
    public static final String LOGIN = ROOT_URL + "login";
    public static final String ORDERS = ROOT_URL + "orders";
    public static final String ORDER_DETAILS = ROOT_URL + "shopper_previous_order";
    public static final String MENU_ITEM_DETAILS = ROOT_URL + "shopper_menu";
    public static final String EDIT_ORDER = ROOT_URL + "create_order";
    public static final String DELETE_ITEM = ROOT_URL + "delete_order_item";
    public static final String DELETE_ORDER = ROOT_URL + "delete_order";
    public static final String MENU_CATEGORIES = ROOT_URL + "shopper_menu_categories";
    public static final String BRANCH_MENU = ROOT_URL + "shopper_menus";
    public static final String TABLES = ROOT_URL + "reservations";
    public static final String EDIT_RESERVATION = ROOT_URL + "edit_branch_reservation";
    public static final String DELETE_RESERVATION = ROOT_URL + "delete_reservation";
    public static final String PROFILE = ROOT_URL + "profile";
    public static final String SET_DEV_IP = ROOT_URL + "shopper_set_device_id";

}
