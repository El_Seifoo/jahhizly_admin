package com.example.seif.jahhizlyadmin2.Tables;

import java.io.Serializable;

/**
 * Created by seif on 1/9/2018.
 */
public class ReservedTables implements Serializable {
    private int id;
    private int userId;
    private int branchId;
    private int orderId;
    private String status;
    private String reservationTime;
    private int chairsCount;
    private String photo;
    private String branchName;
    private int tablePlace;
    private int guestType;
    private String note;
    private int maxGuests;
    private String openTime;
    private String closeTime;


    public ReservedTables(int id, int userId, int branchId, int orderId,
                          String status, String reservationTime, int chairsCount,
                          String photo, int tablePlace, int guestType, String note, int maxGuests, String openTime, String closeTime) {
        this.id = id;
        this.userId = userId;
        this.branchId = branchId;
        this.orderId = orderId;
        this.status = status;
        this.reservationTime = reservationTime;
        this.chairsCount = chairsCount;
        this.photo = photo;
        this.tablePlace = tablePlace;
        this.guestType = guestType;
        this.note = note;
        this.maxGuests = maxGuests;
        this.openTime = openTime;
        this.closeTime = closeTime;
    }


    public int getTablePlace() {
        return tablePlace;
    }

    public void setTablePlace(int tablePlace) {
        this.tablePlace = tablePlace;
    }

    public int getGuestType() {
        return guestType;
    }

    public void setGuestType(int guestType) {
        this.guestType = guestType;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getMaxGuests() {
        return maxGuests;
    }

    public void setMaxGuests(int maxGuests) {
        this.maxGuests = maxGuests;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }


    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReservationTime() {
        return reservationTime;
    }

    public void setReservationTime(String reservationTime) {
        this.reservationTime = reservationTime;
    }

    public int getChairsCount() {
        return chairsCount;
    }

    public void setChairsCount(int chairsCount) {
        this.chairsCount = chairsCount;
    }

}
