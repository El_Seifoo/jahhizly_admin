package com.example.seif.jahhizlyadmin2.Utils;

/**
 * Created by seif on 8/13/2017.
 */
public class Constants {


    public static final String SHARED_PREF_NAME = "jahhizly";
    public static final String USER_TOKEN = "token";
    public static final String USER_DATA = "userData";
    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String IS_CART_FULL = "is_cart_full";
    public static final String APP_LANG = "appLang";


}
