package com.example.seif.jahhizlyadmin2.Home.Orders;

import java.io.Serializable;

/**
 * Created by seif on 1/4/2018.
 */
public class Orders implements Serializable {
    // id- , branch_id- , user_id- , reservation_id- , status- , price- , createdAt
    private int id;
    private int userId;
    private int branchId;
    private int reservationId;
    private String status;
    private double price;
    private String date;
    private String time;
    private int preparationTime;
    private String photo;
    private String branchName;

    @Override
    public String toString() {
        return id + " - " + userId + " - " + branchId + " - " + reservationId + " - " + status + " - " + price + " - " + date + " - " + time + " - " + preparationTime + " - " + photo + " - " + branchName;
    }

    public Orders(int id, int userId, int branchId, int reservationId, String status, double price, String date, String time, int preparationTime, String photo) {
        this.id = id;
        this.userId = userId;
        this.branchId = branchId;
        this.reservationId = reservationId;
        this.status = status;
        this.price = price;
        this.date = date;
        this.time = time;
        this.preparationTime = preparationTime;
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getReservationId() {
        return reservationId;
    }

    public void setReservationId(int reservationId) {
        this.reservationId = reservationId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(int preparationTime) {
        this.preparationTime = preparationTime;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }


}
