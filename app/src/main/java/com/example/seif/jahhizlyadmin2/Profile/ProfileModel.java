package com.example.seif.jahhizlyadmin2.Profile;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.seif.jahhizlyadmin2.Utils.MySingleton;
import com.example.seif.jahhizlyadmin2.Utils.URLs;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by seif on 1/8/2018.
 */
public class ProfileModel {
    public ProfileModel() {
    }

    protected void profile(final Context context, final VolleyCallback callback) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URLs.PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Profile response", response);
                        try {
                            callback.onSuccess(context, response);
                        } catch (JSONException e) {
                            Log.e("Profile parsing error", e.toString());
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Profile error", error.toString());
                        callback.onFail(context, error);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected interface VolleyCallback {
        void onSuccess(Context context, String response) throws JSONException;

        void onFail(Context context, VolleyError error);
    }
}
