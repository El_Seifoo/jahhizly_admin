package com.example.seif.jahhizlyadmin2.Tables;

import android.content.Context;

import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.example.seif.jahhizlyadmin2.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by seif on 1/8/2018.
 */
public class TablesPresenter implements TablesMVP.presenter, TablesModel.VolleyCallback {

    private TablesModel model;
    private TablesMVP.view view;

    public TablesPresenter(TablesModel model, TablesMVP.view view) {
        this.model = model;
        this.view = view;
    }

    @Override
    public void onTablesCreated(Context context, int page, String status, String key) {
        if (page > 0) {
            view.showLoadingMoreProgress();
            model.requestTables(context, this, page, status, key);
        } else {
            view.showProgress();
            model.requestTables(context, this, page, status, key);
        }

    }

    @Override
    public void onTablesCreated(Context context, String status, String key) {
        model.requestTables(context, this, status, key);

    }

    @Override
    public void onSuccess(Context context, String response, int page) throws JSONException {
        if (page > 0) {
            view.hideLoadingMoreProgress();
        } else {
            view.hideProgress();
        }
        ArrayList<ReservedTables> tables = new ArrayList<>();
        JSONObject jsonResponse = new JSONObject(response);
        JSONArray reservations = jsonResponse.getJSONArray("reservations");
        for (int i = 0; i < reservations.length(); i++) {
            int id = reservations.getJSONObject(i).getInt("id");
            int userId = reservations.getJSONObject(i).getInt("user_id");
            int branchId = reservations.getJSONObject(i).getInt("branch_id");

            int orderId;
            if (reservations.getJSONObject(i).getString("order_id").equals("") ||
                    reservations.getJSONObject(i).getString("order_id").equals("null")) {
                orderId = -1;
            } else {
                orderId = reservations.getJSONObject(i).getInt("order_id");
            }
            int statusInt = reservations.getJSONObject(i).getInt("status");
            String status = "";
            if (statusInt == 0) {
                status = context.getString(R.string.status_0_1);
            }
//            else if (statusInt == 1) {
//                status = context.getString(R.string.status_1_1);
//            }
            else if (statusInt == 2) {
                status = context.getString(R.string.status_2_1);
            } else if (statusInt == 3) {
                status = context.getString(R.string.status_3_1);
            } else if (statusInt == 4) {
                status = context.getString(R.string.status_4_1);
            }
            String reservationTime;
            if (reservations.getJSONObject(i).getString("reservation_time").equals("") ||
                    reservations.getJSONObject(i).getString("reservation_time").equals("null")) {
                reservationTime = "null";
            } else {
                reservationTime = reservations.getJSONObject(i).getString("reservation_time");
            }
            int chairsCount = reservations.getJSONObject(i).getInt("chairs_count");
            int tablePlace = reservations.getJSONObject(i).getInt("table_place");
            int guestType = reservations.getJSONObject(i).getInt("guest_type");
            String note;
            if (reservations.getJSONObject(i).getString("note").equals("") || reservations.getJSONObject(i).getString("note").equals("null")) {
                note = "";
            } else {
                note = reservations.getJSONObject(i).getString("note");
            }
            int maxGuests = reservations.getJSONObject(i).getInt("max_guests");
            String openTime = reservations.getJSONObject(i).getString("open_time");
            String closeTime = reservations.getJSONObject(i).getString("close_time");
            tables.add(new ReservedTables(id, userId, branchId, orderId, status, reservationTime, chairsCount, "", tablePlace, guestType, note, maxGuests, openTime, closeTime));
        }
        if (tables.size() > 0) {
            view.loadData(tables);
        } else {
            if (page == 0)
                view.showEmptyTxt();
        }
    }


    @Override
    public void onFail(Context context, VolleyError error, int page) {
        if (page != -1) {
            if (page > 0) {
                view.hideLoadingMoreProgress();
            } else {
                view.hideProgress();
            }
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                view.showMessage(context.getString(R.string.internet_error));
            } else if (error instanceof ServerError) {
                view.showMessage(context.getString(R.string.server_error));
            } else {
                view.showMessage(context.getString(R.string.wrong));
            }
        }
    }
}
