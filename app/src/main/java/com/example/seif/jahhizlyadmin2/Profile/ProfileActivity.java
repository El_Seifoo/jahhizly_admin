package com.example.seif.jahhizlyadmin2.Profile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.seif.jahhizlyadmin2.R;
import com.example.seif.jahhizlyadmin2.Utils.MySingleton;
import com.example.seif.jahhizlyadmin2.Utils.URLs;
import com.tuyenmonkey.mkloader.MKLoader;

import de.hdodenhof.circleimageview.CircleImageView;
import me.anwarshahriar.calligrapher.Calligrapher;

public class ProfileActivity extends AppCompatActivity implements ProfileMVP.view {
    ProfilePresenter presenter;
    MKLoader loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Calligrapher calligrapher = new Calligrapher(this);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(this, "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(this, "fonts/English/Roboto_Regular.ttf", true);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter = new ProfilePresenter(new ProfileModel(), this);
        loading = (MKLoader) findViewById(R.id.loading);
        presenter.requestUserData(this);

    }

    @Override
    public void setUserData(UserInfo userInfo) {

        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            ((TextView) findViewById(R.id.user_name)).setText(convertDigits(userInfo.getName()));
            ((TextView) findViewById(R.id.branch_name)).setText(convertDigits(userInfo.getBranchArabicName()));
        } else {
            ((TextView) findViewById(R.id.user_name)).setText(convertDigits(userInfo.getName()));
            ((TextView) findViewById(R.id.branch_name)).setText(userInfo.getBranchName());
        }
        Glide.with(this).load(URLs.ROOT_URL + userInfo.getBrandLogo())
                .error(R.mipmap.restaurant_meal_icon)
                .thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(((CircleImageView) findViewById(R.id.brand_logo)));
    }

    @Override
    public void showProgress() {
        ((FrameLayout) findViewById(R.id.container)).setVisibility(View.INVISIBLE);
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        ((FrameLayout) findViewById(R.id.container)).setVisibility(View.VISIBLE);
        loading.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}
