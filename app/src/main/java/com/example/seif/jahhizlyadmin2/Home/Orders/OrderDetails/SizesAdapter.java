package com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.seif.jahhizlyadmin2.R;

import java.util.ArrayList;

import me.anwarshahriar.calligrapher.Calligrapher;

/**
 * Created by seif on 1/4/2018.
 */
public class SizesAdapter extends RecyclerView.Adapter<SizesAdapter.Holder> {
    ArrayList<String> size;
    ArrayList<Double> price = new ArrayList<>();
    Context context;
    final private SizesListItemClickListener mOnClickListener;
    private boolean flag;


    public interface SizesListItemClickListener {
        void onSizeListItemClicked(int position);
    }

    public SizesAdapter(Context context, ArrayList<String> size, ArrayList<Double> price, SizesListItemClickListener listener, boolean flag) {
        this.context = context;
        this.size = size;
        this.price = price;
        mOnClickListener = listener;
        this.flag = flag;
    }

    @Override
    public SizesAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.sizes_layout, parent, false);
        return new Holder(view);
    }

    private int selectedPosition;

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    @Override
    public void onBindViewHolder(final SizesAdapter.Holder holder, final int position) {
        holder.sizeTxt.setText(flag ? convertDigits(" " + size.get(position) + " ") : " " + size.get(position) + " ");
        holder.priceTxt.setText(flag ? convertDigits(" " + price.get(position) + holder.itemView.getContext().getString(R.string.currency)) : " " + price.get(position) + holder.itemView.getContext().getString(R.string.currency));
        if (position == selectedPosition) {
            holder.sizeTxt.setTextColor(holder.itemView.getContext().getResources().getColor(R.color.white));
            holder.priceTxt.setTextColor(holder.itemView.getContext().getResources().getColor(R.color.white));
            holder.itemView.setBackground(new ColorDrawable(holder.itemView.getContext().getResources().getColor(R.color.red_3)));
        } else {
            holder.sizeTxt.setTextColor(holder.itemView.getContext().getResources().getColor(R.color.black));
            holder.priceTxt.setTextColor(holder.itemView.getContext().getResources().getColor(R.color.green_1));
            holder.itemView.setBackground(new ColorDrawable(holder.itemView.getContext().getResources().getColor(R.color.white)));
        }


    }

    @Override
    public int getItemCount() {
        return size.size();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView sizeTxt, priceTxt;
        View root;

        public Holder(View itemView) {
            super(itemView);
            root = itemView;
            sizeTxt = (TextView) itemView.findViewById(R.id.size_name);
            priceTxt = (TextView) itemView.findViewById(R.id.size_price);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            selectedPosition = position;
            notifyDataSetChanged();
            mOnClickListener.onSizeListItemClicked(position);
        }
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}

