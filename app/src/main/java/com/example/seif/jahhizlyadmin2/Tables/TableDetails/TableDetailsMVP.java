package com.example.seif.jahhizlyadmin2.Tables.TableDetails;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;

import com.example.seif.jahhizlyadmin2.Tables.ReservedTables;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by seif on 1/9/2018.
 */
public interface TableDetailsMVP {
    interface view {

        void showProgress();

        void hideProgress();

        void showMessage(String message);

        void editionDone(String message);

        void setTableData(ReservedTables table);
    }

    interface presenter {
        void onTableDetailsCreated(ReservedTables table);

        void onConfirmBtnClicked(Context context, int reservationId, int userId, int branchId, String tablePlace, String guestType, String note, String chairsCount, String time, int orderId, int status) throws JSONException;
    }
}
