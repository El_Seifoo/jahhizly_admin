package com.example.seif.jahhizlyadmin2.Menu;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.LayoutDirection;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails.ItemExtrasDetails;
import com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails.ItemSizeDetails;
import com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails.OrdersDetails;
import com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails.SizesAdapter;
import com.example.seif.jahhizlyadmin2.R;
import com.example.seif.jahhizlyadmin2.Utils.MySingleton;
import com.example.seif.jahhizlyadmin2.Utils.URLs;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

/**
 * Created by seif on 1/7/2018.
 */
public class MenuFragment extends Fragment implements MenuMVP.menuView, MenuAdapter.ListItemClickListener, SizesAdapter.SizesListItemClickListener {
    MKLoader loading, dialogLoading;
    TextView emptyTxt;
    MenuPresenter presenter;
    RecyclerView menuRecView;
    MenuAdapter adapter;
    ArrayList<MenuItem> allMenus;
    ArrayList<String> sizes = new ArrayList<>();
    ArrayList<Double> prices = new ArrayList<>();
    ArrayList<Integer> sizesIds = new ArrayList<>();
    private String size;
    private double price;
    private int sizeId;
    int counterInteger = 1;
    double extraPrice = 0;
    static ArrayList<OrdersDetails> addedItems = new ArrayList<>();

    public MenuFragment() {
    }

    public static MenuFragment newInstance(int categoryId, int branchId) {
        MenuFragment fragment = new MenuFragment();
        Bundle args = new Bundle();
        args.putInt("CategoryID", categoryId);
        args.putSerializable("BranchID", branchId);
        fragment.setArguments(args);
        return fragment;
    }

    public static ArrayList<OrdersDetails> getAddedItems() {
        return addedItems;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        design(view);
        loading = (MKLoader) view.findViewById(R.id.loading);
        emptyTxt = (TextView) view.findViewById(R.id.empty_list_text);
        presenter = new MenuPresenter(new MenuModel(), this,
                MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
        menuRecView = (RecyclerView) view.findViewById(R.id.menus_recycler_view);
        RecyclerView.LayoutManager layout = new GridLayoutManager(getActivity().getApplicationContext(), 2);
        menuRecView.setHasFixedSize(true);
        menuRecView.setNestedScrollingEnabled(false);
        menuRecView.setLayoutManager(layout);
        adapter = new MenuAdapter(this, MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
        presenter.requestBranchMenu(getContext(), getArguments().getInt("BranchID"), getArguments().getInt("CategoryID"));
        return view;
    }

    private void design(View view) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(params);
    }

    @Override
    public void showProgress() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        loading.setVisibility(View.GONE);
    }

    @Override
    public void loadData(ArrayList<MenuItem> menus) {
        allMenus = menus;
        adapter.setMenuData(menus);
        ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
        scaleInAnimationAdapter.setFirstOnly(false);
        menuRecView.setAdapter(scaleInAnimationAdapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEmptyTxt() {
        emptyTxt.setVisibility(View.VISIBLE);
    }

    @Override
    public void onListItemClick(int clickedItemIndex) {
        presenter.onMenuListItemClicked(new Dialog(getContext()), allMenus.get(clickedItemIndex).getId(), clickedItemIndex);
    }

    TextView counter, cartDialogPrice;

    @Override
    public void createDialog(final Dialog dialog, int menuId, final int index) {
        dialog.setContentView(R.layout.menu_item_dialoge);
        dialogLoading = (MKLoader) dialog.findViewById(R.id.dialog_loading);
        presenter.requestSizesExtras(getContext(), dialog, menuId, index);
        ImageView increase = (ImageView) dialog.findViewById(R.id.cart_dialog_increase);
        ImageView decrease = (ImageView) dialog.findViewById(R.id.cart_dialog_decrease);
        counter = (TextView) dialog.findViewById(R.id.cart_dialog_counter);
        TextView cartDialogName = (TextView) dialog.findViewById(R.id.menu_item_name);
        TextView cartDialogTime = (TextView) dialog.findViewById(R.id.cart_dialog_time);
        cartDialogPrice = (TextView) dialog.findViewById(R.id.cart_dialog_price);
        String priceTxt = getString(R.string.price) +
                "<font color = '#65db73'>" + String.valueOf((extraPrice * getQuantity()) + (price * getQuantity())) + getString(R.string.currency) + "</font>";
        cartDialogPrice.setText(Html.fromHtml(priceTxt), TextView.BufferType.SPANNABLE);
        CircleImageView cartDialogImg = (CircleImageView) dialog.findViewById(R.id.menu_item_img);
        cartDialogName.setText(" " + allMenus.get(index).getName() + " ");
        cartDialogTime.setText(" " + allMenus.get(index).getTime() + getString(R.string.time));
        Glide.with(getContext()).load(URLs.ROOT_URL + allMenus.get(index).getPhoto())
                .error(R.mipmap.restaurant_meal_icon)
                .thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(cartDialogImg);
        increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onPlusClicked();
            }
        });

        decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onMinusClicked();
            }
        });

        final Button addItem = (Button) dialog.findViewById(R.id.edit_btn);
        addItem.setText(getContext().getString(R.string.add_item));
        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // adding item functions ....
                addedItems.add(new OrdersDetails(getQuantity(), getPrice(), allMenus.get(index).getName(), getExtrasId(), getExtras(), "", getSizeId(), getSize(), allMenus.get(index).getId()));
                dialog.dismiss();
                presenter.onAddItemClicked(dialog, new Dialog(getContext()), addedItems);
                for (int i = 0; i < addedItems.size(); i++) {
                    Log.e("list", addedItems.get(i).getQuantity() + "//" + addedItems.get(i).getItemName() + "//" + addedItems.get(i).getExtraName() + "//" + addedItems.get(i).getSize() + "//" + addedItems.get(i).getMenuId());
                }
            }
        });

        Button cancel = (Button) dialog.findViewById(R.id.dialog_cancel_btn);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (sizes != null && sizes.size() > 0) {
                    sizes.clear();
                }
                if (prices != null && prices.size() > 0) {
                    prices.clear();
                }
                if (extras != null && extras.size() > 0) {
                    extras.clear();
                }
                extraPrice = 0;
                size = "";
                price = 0;
                counterInteger = 1;

            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
//        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        DisplayMetrics metrics = getResources().getDisplayMetrics();
//        metrics.widthPixels, metrics.heightPixels
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                dialog.getWindow().setLayout(2 * (metrics.widthPixels) / 3, LinearLayout.LayoutParams.WRAP_CONTENT);
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                dialog.getWindow().setLayout(2 * (metrics.widthPixels) / 2, LinearLayout.LayoutParams.WRAP_CONTENT);
                break;
            default:
                dialog.getWindow().setLayout((metrics.widthPixels), LinearLayout.LayoutParams.WRAP_CONTENT);
        }
        dialog.show();
    }

    @Override
    public void showDialogProgress(Dialog dialog) {
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        dialogLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideDialogProgress(Dialog dialog) {
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        dialogLoading.setVisibility(View.GONE);
    }

    SizesAdapter sizesAdapter;

    @Override
    public void sizesList(Dialog dialog, ArrayList<ItemSizeDetails> itemSizeDetails, int index) {
        for (int i = 0; i < itemSizeDetails.size(); i++) {
            size = itemSizeDetails.get(0).getSize();
            price = itemSizeDetails.get(0).getPrice();
            sizeId = itemSizeDetails.get(0).getId();
            sizes.add(itemSizeDetails.get(i).getSize());
            prices.add(itemSizeDetails.get(i).getPrice());
            sizesIds.add(itemSizeDetails.get(i).getId());
        }
        String priceTxt = getString(R.string.price) +
                "<font color = '#65db73'>" + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency) + "</font>";
        cartDialogPrice.setText(Html.fromHtml(priceTxt), TextView.BufferType.SPANNABLE);
        itemSizeDetails.clear();
        RecyclerView rv = (RecyclerView) dialog.findViewById(R.id.sizes_rec_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rv.setLayoutManager(layoutManager);
        rv.setHasFixedSize(true);
        sizesAdapter = new SizesAdapter(getContext(), sizes, prices, this, MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
        rv.setAdapter(sizesAdapter);
    }

    LinearLayout container;
    ArrayList<ItemExtrasDetails> extras;

    @Override
    public void checkBoxExtra(Dialog dialog, ArrayList<ItemExtrasDetails> itemExtrasDetails, int index) {
        extras = itemExtrasDetails;
        container = (LinearLayout) dialog.findViewById(R.id.extras_container);
        for (int i = 0; i < extras.size(); i++) {
            LinearLayout linearLayout = new LinearLayout(getContext());
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            final CheckBox checkBox = new CheckBox(new ContextThemeWrapper(getContext(), R.style.customRadioBtn));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            checkBox.setLayoutParams(params);
            checkBox.setTag(extras.get(i).getPrice());
            checkBox.setText(extras.get(i).getName());
            if (getResources().getConfiguration().getLayoutDirection() == LayoutDirection.RTL) {
                checkBox.setGravity(Gravity.RIGHT | Gravity.CENTER);
            }
            checkBox.setTextSize(15);
            checkBox.setChecked(false);
            checkBox.setTextColor(getResources().getColor(R.color.grey_5));
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (isChecked) {
                        extraPrice += Double.parseDouble(String.valueOf(compoundButton.getTag()));
                        String priceTxt = getString(R.string.price) +
                                "<font color = '#65db73'>" + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency) + "</font>";
                        cartDialogPrice.setText(Html.fromHtml(priceTxt), TextView.BufferType.SPANNABLE);

                    } else {
                        extraPrice -= Double.parseDouble(String.valueOf(compoundButton.getTag()));
                        String priceTxt = getString(R.string.price) +
                                "<font color = '#65db73'>" + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency) + "</font>";
                        cartDialogPrice.setText(Html.fromHtml(priceTxt), TextView.BufferType.SPANNABLE);
                    }
                }
            });
            linearLayout.addView(checkBox, 0);

            TextView txt = new TextView(getContext());
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            checkBox.setLayoutParams(params1);
            txt.setText(String.valueOf(extras.get(i).getPrice()) + getString(R.string.currency));
            txt.setTextColor(getResources().getColor(R.color.green_1));
            txt.setTextSize(15);
            txt.setGravity(Gravity.START);
            linearLayout.addView(txt, 1);
            container.addView(linearLayout);
        }


    }

    @Override
    public void increase() {
        if (counterInteger >= 99) {
            counterInteger = 99;
        } else {
            counterInteger++;
        }
        counter.setText(String.valueOf(counterInteger));
        String priceTxt = getString(R.string.price) +
                "<font color = '#65db73'>" + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency) + "</font>";
        cartDialogPrice.setText(Html.fromHtml(priceTxt), TextView.BufferType.SPANNABLE);
    }

    @Override
    public void decrease() {
        if (counterInteger <= 1) {
            counterInteger = 1;
        } else {
            counterInteger--;
        }
        counter.setText(String.valueOf(counterInteger));
        String priceTxt = getString(R.string.price) +
                "<font color = '#65db73'>" + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency) + "</font>";
        cartDialogPrice.setText(Html.fromHtml(priceTxt), TextView.BufferType.SPANNABLE);
    }

    @Override
    public int getQuantity() {
        return counterInteger;
    }

    @Override
    public int getSizeId() {
        return sizeId;
    }

    @Override
    public String getSize() {
        return size;
    }

    double extrasPrice = 0;

    @Override
    public String getExtrasId() {
        String extrasId = "";
        ArrayList<String> extrasData = getExtrasData();
        for (int i = 0; i < extras.size(); i++) {
            for (int y = 0; y < extrasData.size(); y++) {
                if (extras.get(i).getName().equals(extrasData.get(y))) {
                    extrasId += extras.get(i).getId();
                    extrasPrice += extras.get(i).getPrice();
                    if (y != extras.size() - 1) {
                        extrasId += ",";
                    }
                }
            }
        }
        return extrasId;
    }


    @Override
    public String getExtras() {
        String extrasNames = "";
        if (getExtrasData() != null && getExtrasData().size() > 0) {
            ArrayList<String> extras = getExtrasData();
            for (int i = 0; i < extras.size(); i++) {
                extrasNames += extras.get(i);
                if (i != extras.size() - 1) {
                    extrasNames += ",";
                }
            }
        }
        return extrasNames;
    }


    private ArrayList<String> getExtrasData() {
        ArrayList<String> checkBoxData = new ArrayList<>();
        int extraSize = container.getChildCount();
        for (int i = 0; i < extraSize; i++) {
            CheckBox extraCheckBox = (CheckBox) ((LinearLayout) container.getChildAt(i)).getChildAt(0);
            if (extraCheckBox.isChecked()) {
                checkBoxData.add(extraCheckBox.getText().toString().trim());
            }
        }
        return checkBoxData;
    }

    @Override
    public double getPrice() {
//        double one = price + extraPrice;
//        double two = one * getQuantity();
        return price;
    }

    @Override
    public void showDialogOptions(final Dialog mainDialog, final Dialog dialog, final ArrayList<OrdersDetails> addedItems) {
        dialog.setContentView(R.layout.continue_cancel_dialog);
        ((TextView) dialog.findViewById(R.id.dialog_ok_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
        ((TextView) dialog.findViewById(R.id.dialog_cancel_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent returnIntent = new Intent();

                for (OrdersDetails ordersDetails : addedItems) {
                    Log.e("menuFragment", ordersDetails.getExtraName());
                }
                returnIntent.putExtra("result", addedItems);
                if (getContext() instanceof MenuActivity) {
                    getActivity().setResult(Activity.RESULT_OK, returnIntent);
                    getActivity().finish();
                    addedItems.clear();
                }

            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        DisplayMetrics metrics = getResources().getDisplayMetrics();
//        metrics.widthPixels, metrics.heightPixels
        dialog.show();
    }


    @Override
    public void onSizeListItemClicked(int position) {
        size = sizes.get(position);
        price = prices.get(position);
        sizeId = sizesIds.get(position);
        String priceTxt = getString(R.string.price) +
                "<font color = '#65db73'>" + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency) + "</font>";
        cartDialogPrice.setText(Html.fromHtml(priceTxt), TextView.BufferType.SPANNABLE);
    }

}
