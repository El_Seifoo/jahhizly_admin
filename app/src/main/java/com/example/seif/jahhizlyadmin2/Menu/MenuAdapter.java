package com.example.seif.jahhizlyadmin2.Menu;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.seif.jahhizlyadmin2.R;
import com.example.seif.jahhizlyadmin2.Utils.URLs;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by seif on 1/7/2018.
 */
public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.Holder> {

    private ArrayList<MenuItem> menu;
    final private ListItemClickListener mOnClickListener;
    private boolean flag;

    public MenuAdapter(ListItemClickListener mOnClickListener, boolean flag) {
        this.mOnClickListener = mOnClickListener;
        this.flag = flag;
    }

    public interface ListItemClickListener {
        void onListItemClick(int clickedItemIndex);
    }

    public void setMenuData(ArrayList<MenuItem> menu) {
        this.menu = menu;
        notifyDataSetChanged();
    }

    @Override
    public MenuAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menu_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(MenuAdapter.Holder holder, int position) {
        Glide.with(holder.itemView.getContext().getApplicationContext())
                .load(URLs.ROOT_URL + menu.get(position).getPhoto())
                .error(R.mipmap.restaurant_meal_icon)
                .thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.menuItemImg);
        holder.menuItemName.setText(flag ? convertDigits(" " + menu.get(position).getName() + " ") : " " + menu.get(position).getName() + " ");
        holder.menuItemPrice.setText(flag ? convertDigits(" " + menu.get(position).getPrice() + holder.itemView.getContext().getString(R.string.currency)) : " " + menu.get(position).getPrice() + holder.itemView.getContext().getString(R.string.currency));
    }

    @Override
    public int getItemCount() {
        return (menu.size() > 0) ? menu.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CircleImageView menuItemImg;
        TextView menuItemName, menuItemPrice;

        public Holder(View itemView) {
            super(itemView);
            menuItemImg = (CircleImageView) itemView.findViewById(R.id.menu_item_img);
            menuItemName = (TextView) itemView.findViewById(R.id.menu_item_name);
            menuItemPrice = (TextView) itemView.findViewById(R.id.menu_item_price);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            mOnClickListener.onListItemClick(position);
        }
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}
