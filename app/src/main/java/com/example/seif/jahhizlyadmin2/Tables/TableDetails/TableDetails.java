package com.example.seif.jahhizlyadmin2.Tables.TableDetails;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Vibrator;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.seif.jahhizlyadmin2.Home.MainActivity;
import com.example.seif.jahhizlyadmin2.R;
import com.example.seif.jahhizlyadmin2.Tables.ReservedTables;
import com.example.seif.jahhizlyadmin2.Tables.TablesModel;
import com.example.seif.jahhizlyadmin2.Utils.MySingleton;
import com.example.seif.jahhizlyadmin2.Utils.URLs;
import com.shawnlin.numberpicker.NumberPicker;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import me.anwarshahriar.calligrapher.Calligrapher;

public class TableDetails extends AppCompatActivity implements TableDetailsMVP.view {
    private ArrayList<String> guests, date, time;
    private String selectedGuests, selectedDate, selectedTime;
    private TableDetailsMVP.presenter presenter;
    private ReservedTables reservedTables;
    int status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_details);
        Calligrapher calligrapher = new Calligrapher(this);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(this, "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(this, "fonts/English/Roboto_Regular.ttf", true);
        }
        getSupportActionBar().setTitle(getString(R.string.book_table_activity));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        reservedTables = (ReservedTables) getIntent().getExtras().getSerializable("ReservationData");
        presenter = new TableDetailsPresenter(this, new TableDetailsModel());
        presenter.onTableDetailsCreated(reservedTables);

    }

    @Override
    public void setTableData(ReservedTables table) {
        CircleImageView branchLogo;
        TextView branchName;
        branchName = (TextView) findViewById(R.id.book_table_branch_name);
        branchName.setText(MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))?convertDigits(table.getBranchName()):table.getBranchName());
        branchLogo = (CircleImageView) findViewById(R.id.book_table_brand_logo);
        Glide.with(this).load(URLs.ROOT_URL + table.getPhoto())
                .thumbnail(0.5f)
                .error(R.mipmap.restaurant_meal_icon)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(branchLogo);

        // manage number of guests numberPicker
        guests = new ArrayList<>();
        for (int i = 0; i < table.getMaxGuests(); i++) {
            if ((i + 1) == 1) {
                guests.add(getString(R.string.guests_one));
            } else if ((i + 1) == 2) {
                guests.add(getString(R.string.guests_two));
            } else if (((i + 1) > 2) && (i + 1) <= 10) {
                guests.add((i + 1) + " " + getString(R.string.guests_three_ten));
            } else {
                guests.add((i + 1) + " " + getString(R.string.guests_11_100));
            }
        }

        if (table.getChairsCount() == 1) {
            selectedGuests = "1";
            createPicker(guests, getString(R.string.guests_one), 0);
        } else if (table.getChairsCount() == 2) {
            selectedGuests = "2";
            createPicker(guests, getString(R.string.guests_two), 0);
        } else {
            selectedGuests = convertNumbers(String.valueOf(table.getChairsCount()), 0);
            createPicker(guests, table.getChairsCount() + " " + getString(R.string.guests_11_100), 0);
        }


        // manage Date numberPicker
        date = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("EEE, d MMM");
        date.add(dateFormat.format(calendar.getTime()));
        for (int i = 1; i < 90; i++) {
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            date.add(dateFormat.format(calendar.getTime()));
        }
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(Integer.valueOf(table.getReservationTime().split(" ")[0].split("-")[0]), Integer.valueOf(table.getReservationTime().split(" ")[0].split("-")[1]), Integer.valueOf(table.getReservationTime().split(" ")[0].split("-")[2]),
                Integer.valueOf(table.getReservationTime().split(" ")[1].split(":")[0]), Integer.valueOf(table.getReservationTime().split(" ")[1].split(":")[1]), Integer.valueOf(table.getReservationTime().split(" ")[1].split(":")[2]));
        calendar1.add(Calendar.MONTH, -1);
        String dateString = new SimpleDateFormat("EEE, d MMM").format(calendar1.getTime());
        selectedDate = convertNumbers(returnDate(dateString), 1);
        createPicker(date, dateString, 1);

        // manage Open-Close time numberPicker
        time = new ArrayList<>();
        Calendar calendarStart = Calendar.getInstance();
        Calendar calendarEnd = Calendar.getInstance();
        String endTime = "";
        String openTime = table.getOpenTime();
        String closeTime = table.getCloseTime();

        calendarStart.set(1, 1, 1, Integer.valueOf(openTime.split(":")[0]), Integer.valueOf(openTime.split(":")[1]));
        calendarEnd.set(1, 1, 1, Integer.valueOf(closeTime.split(":")[0]), Integer.valueOf(closeTime.split(":")[1]));

        while (!endTime.equals(new SimpleDateFormat("h:mm a").format(calendarEnd.getTime()))) {
            calendarStart.add(Calendar.MINUTE, 15);
            String generatedTime = new SimpleDateFormat("h:mm a").format(calendarStart.getTime());
            time.add(generatedTime);
            endTime = generatedTime;
        }
        String timeString = new SimpleDateFormat("h:mm a").format(calendar1.getTime());
        selectedTime = convertNumbers(convertTo24H(timeString), 2);
        createPicker(time, timeString, 2);

        ((RadioButton) ((RadioGroup) findViewById(R.id.tables_place_radio_group)).getChildAt(table.getTablePlace())).setChecked(true);
        ((RadioButton) ((RadioGroup) findViewById(R.id.family_not_radio_group)).getChildAt(table.getGuestType())).setChecked(true);
        ((EditText) findViewById(R.id.add_note_edit_text)).setText(table.getNote().trim());
        // spinner status ....
//        status = (table.getStatus().equals(getString(R.string.status_0_1))) ? 0 : (table.getStatus().equals(getString(R.string.status_1_1))) ? 1 : (reservedTables.getStatus().equals(getString(R.string.status_2_1))) ? 2 : (reservedTables.getStatus().equals(getString(R.string.status_3_1))) ? 3 : 4;
        status = (table.getStatus().equals(getString(R.string.status_0_1))) ? 0 : (reservedTables.getStatus().equals(getString(R.string.status_2_1))) ? 2 : (reservedTables.getStatus().equals(getString(R.string.status_3_1))) ? 3 : 4;
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
//        final String[] statusString = {getString(R.string.status_0_1), getString(R.string.status_1_1), getString(R.string.status_2_1), getString(R.string.status_3_1), getString(R.string.status_4_1)};
        final String[] statusString = {getString(R.string.status_0_1), getString(R.string.status_2_1), getString(R.string.status_3_1), getString(R.string.status_4_1)};
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item,
                        statusString); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setSelection(status);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
//                status = (statusString[position].equals(getString(R.string.status_0_1))) ? 0 : (statusString[position].equals(getString(R.string.status_1_1))) ? 1 : (statusString[position].equals(getString(R.string.status_2_1))) ? 2 : (statusString[position].equals(getString(R.string.status_3_1))) ? 3 : 4;
                status = (statusString[position].equals(getString(R.string.status_0_1))) ? 0 : (statusString[position].equals(getString(R.string.status_2_1))) ? 2 : (statusString[position].equals(getString(R.string.status_3_1))) ? 3 : 4;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void confirm(View view) {
        try {
            presenter.onConfirmBtnClicked(TableDetails.this, reservedTables.getId(), reservedTables.getUserId(), reservedTables.getBranchId(),
                    getSelectedOption(((RadioGroup) findViewById(R.id.tables_place_radio_group))).equals(getString(R.string.indoor)) ? "0" : "1",
                    getSelectedOption(((RadioGroup) findViewById(R.id.family_not_radio_group))).equals(getString(R.string.family)) ? "0" : "1",
                    ((EditText) findViewById(R.id.add_note_edit_text)).getText().toString().trim(), selectedGuests, selectedDate + " " + selectedTime,
                    reservedTables.getOrderId(), status);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getSelectedOption(RadioGroup radioGroup) {
        int selected = radioGroup.getCheckedRadioButtonId();
        RadioButton selectedRadioButton = (RadioButton) findViewById(selected);
        if (selectedRadioButton == null) {
            return null;
        }
        return String.valueOf(selectedRadioButton.getText());
    }

    private void createPicker(final ArrayList<String> data, String string, final int which) {
        NumberPicker numberPicker;
        if (which == 0) {
            //persons
            numberPicker = (NumberPicker) findViewById(R.id.guest_picker);
        } else if (which == 1) {
            //date
            numberPicker = (NumberPicker) findViewById(R.id.date_picker);
        } else {
            //time
            numberPicker = (NumberPicker) findViewById(R.id.time_picker);
        }
        numberPicker.setMaxValue(data.size());
        numberPicker.setMinValue(1);
        numberPicker.setDisplayedValues(data.toArray(new String[data.size()]));
        numberPicker.setWrapSelectorWheel(false);
        Log.e("string", string);
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).equals(string)) {
                numberPicker.setValue(i + 1);
            }
        }

        // Set divider color
        numberPicker.setDividerColor(ContextCompat.getColor(this, R.color.red_3));
        numberPicker.setDividerColorResource(R.color.red_3);
        // Set selected text color
        numberPicker.setSelectedTextColor(ContextCompat.getColor(this, R.color.red_3));
        numberPicker.setSelectedTextColorResource(R.color.red_3);
        // Set text color
        numberPicker.setTextColor(ContextCompat.getColor(this, R.color.grey_5));
        numberPicker.setTextColorResource(R.color.grey_5);

        // OnValueChangeListener
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if (which == 0) {
                    //persons
                    if (guests.get(newVal - 1).equals(getString(R.string.guests_one))) {
                        selectedGuests = "1";
                    } else if (guests.get(newVal - 1).equals(getString(R.string.guests_two))) {
                        selectedGuests = "2";
                    } else {
                        selectedGuests = convertNumbers(guests.get(newVal - 1).split(" ")[0], 0);
                    }
                    Log.e("guests", selectedGuests);
                } else if (which == 1) {
                    // date
                    // Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec
                    selectedDate = convertNumbers(returnDate(data.get(newVal - 1)), 1);
                    Log.e("date", selectedDate);
                } else if (which == 2) {
                    //time
                    selectedTime = convertNumbers(convertTo24H(time.get(newVal - 1)), 2);
                    Log.e("time", selectedTime);

                }
            }
        });
    }

    private String convertTo24H(String time) {

        try {
            SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
            SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
            Date timeDate = parseFormat.parse(time);
            return displayFormat.format(timeDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String returnDate(String date) {
        String rslt = "";
        Calendar calendarMonth = Calendar.getInstance();
        int currentMonth = calendarMonth.get(Calendar.MONTH) + 1;
        Calendar calendarYear = Calendar.getInstance();
        int year = calendarYear.get(Calendar.YEAR);
        String month = date.split(" ")[2];
        int selectedMonthInt;
        if (month.equals(getResources().getStringArray(R.array.months_of_year)[0])) {
            selectedMonthInt = 1;
            rslt = "-" + 1 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[1])) {
            selectedMonthInt = 2;
            rslt = "-" + 2 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[2])) {
            selectedMonthInt = 3;
            rslt = "-" + 3 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[3])) {
            selectedMonthInt = 4;
            rslt = "-" + 4 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[4])) {
            selectedMonthInt = 5;
            rslt = "-" + 5 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[5])) {
            selectedMonthInt = 6;
            rslt = "-" + 6 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[6])) {
            selectedMonthInt = 7;
            rslt = "-" + 7 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[7])) {
            selectedMonthInt = 8;
            rslt = "-" + 8 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[8])) {
            selectedMonthInt = 9;
            rslt = "-" + 9 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[9])) {
            selectedMonthInt = 10;
            rslt = "-" + 10 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[10])) {
            selectedMonthInt = 11;
            rslt = "-" + 11 + "-" + date.split(" ")[1];
        } else {
            selectedMonthInt = 12;
            rslt = "-" + 12 + "-" + date.split(" ")[1];
        }
        if (currentMonth > selectedMonthInt) {
            rslt = (year + 1) + rslt;
        } else {
            rslt = year + rslt;
        }

        return rslt;
    }

    private String convertNumbers(String number, int type) {
        if (type == 0) {
            //guests
            String guest = "";
            for (int i = 0; i < number.length(); i++) {
                guest += convertDigit(number.charAt(i));
            }
            return guest;
        } else if (type == 1) {
            //date ٢٠١٨-4-٢٠
            String date = "";
            for (int i = 0; i < number.length(); i++) {
                date += convertDigit(number.charAt(i));
            }
            return date;
        } else {
            //time
            String time = "";
            for (int i = 0; i < number.length(); i++) {
                time += convertDigit(number.charAt(i));
            }
            return time;
        }
    }

    private String convertDigit(char digit) {
        switch (digit) {
            case '٠':
                return "0";
            case '١':
                return "1";
            case '٢':
                return "2";
            case '٣':
                return "3";
            case '٤':
                return "4";
            case '٥':
                return "5";
            case '٦':
                return "6";
            case '٧':
                return "7";
            case '٨':
                return "8";
            case '٩':
                return "9";
            default:
                return String.valueOf(digit);

        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        ((MKLoader) findViewById(R.id.loading)).setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        ((MKLoader) findViewById(R.id.loading)).setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void editionDone(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("GoToTables", "GoToTables");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }


}
