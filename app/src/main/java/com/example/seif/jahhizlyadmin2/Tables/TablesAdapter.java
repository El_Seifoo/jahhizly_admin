package com.example.seif.jahhizlyadmin2.Tables;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.seif.jahhizlyadmin2.Home.MainActivity;
import com.example.seif.jahhizlyadmin2.Home.Orders.OrdersContainerFragment;
import com.example.seif.jahhizlyadmin2.Home.Orders.OrdersFragment;
import com.example.seif.jahhizlyadmin2.R;
import com.example.seif.jahhizlyadmin2.Utils.MySingleton;
import com.example.seif.jahhizlyadmin2.Utils.URLs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import me.anwarshahriar.calligrapher.Calligrapher;

/**
 * Created by seif on 1/9/2018.
 */
public class TablesAdapter extends RecyclerView.Adapter<TablesAdapter.Holder> {
    private ArrayList<ReservedTables> tables;
    final private TablesListItemClickListener listener;
    private Activity activity;
    private int width;
    private int screenSize;
    private boolean flag;

    public TablesAdapter(Activity activity, TablesListItemClickListener listener, int width, int screenSize, boolean flag) {
        this.activity = activity;
        this.listener = listener;
        this.width = width;
        this.screenSize = screenSize;
        this.flag = flag;
    }

    public interface TablesListItemClickListener {
        void onListItemClickListener(int position);
    }

    public void setData(ArrayList<ReservedTables> tables) {
        this.tables = tables;
        notifyDataSetChanged();
    }


    @Override
    public TablesAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tables_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final TablesAdapter.Holder holder, final int position) {
        Calligrapher calligrapher = new Calligrapher(holder.itemView.getContext());
        if (flag)
            calligrapher.setFont(holder.itemView, "fonts/Arabic/arabic_font.ttf");
        else
            calligrapher.setFont(holder.itemView, "fonts/English/Roboto_Regular.ttf");

        Glide.with(holder.itemView.getContext()).load(URLs.ROOT_URL + tables.get(position).getPhoto())
                .error(R.mipmap.restaurant_meal_icon)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.5f)
                .into(holder.logo);
        holder.name.setText(flag ? convertDigits(tables.get(position).getBranchName()) : tables.get(position).getBranchName());
        holder.reservationId.setText(flag ? convertDigits(holder.itemView.getContext().getString(R.string.reserv_num) + tables.get(position).getId()) : holder.itemView.getContext().getString(R.string.reserv_num) + tables.get(position).getId());
        holder.chairsCount.setText(flag ? convertDigits(tables.get(position).getChairsCount() + "") : tables.get(position).getChairsCount() + "");
        holder.tableStatus.setText(tables.get(position).getStatus());
        if (tables.get(position).getReservationTime().equals("null")) {
            holder.reservationTime.setVisibility(View.GONE);
        } else {
            String[] time = tables.get(position).getReservationTime().split(" ");
            holder.reservationTime.setText(flag ? convertDigits(" " + time[1].split(":")[0] + ":" + time[1].split(":")[1] + " , " + time[0].split("-")[2] + " - " + time[0].split("-")[1] + " - " + time[0].split("-")[0] + " ") : " " + time[1].split(":")[0] + ":" + time[1].split(":")[1] + " , " + time[0] + " ");
        }
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteReservation(position, holder.itemView.getContext());
            }
        });
        holder.tableStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(holder.itemView.getContext());
                dialog.setContentView(R.layout.status_dialog);
                ((RadioButton) ((RadioGroup) dialog.findViewById(R.id.status_container)).getChildAt(0)).setText(holder.itemView.getContext().getString(R.string.status_0_1));
//                ((RadioButton) ((RadioGroup) dialog.findViewById(R.id.status_container)).getChildAt(1)).setText(holder.itemView.getContext().getString(R.string.status_1_1));
                ((RadioButton) ((RadioGroup) dialog.findViewById(R.id.status_container)).getChildAt(1)).setVisibility(View.GONE);
                ((RadioButton) ((RadioGroup) dialog.findViewById(R.id.status_container)).getChildAt(2)).setText(holder.itemView.getContext().getString(R.string.status_2_1));
                ((RadioButton) ((RadioGroup) dialog.findViewById(R.id.status_container)).getChildAt(3)).setText(holder.itemView.getContext().getString(R.string.status_3_1));
                ((RadioButton) ((RadioGroup) dialog.findViewById(R.id.status_container)).getChildAt(4)).setText(holder.itemView.getContext().getString(R.string.status_4_1));
                final String mainStatus;
                if (holder.tableStatus.getText().equals(holder.itemView.getContext().getString(R.string.status_0_1))) {
                    ((RadioButton) ((RadioGroup) dialog.findViewById(R.id.status_container)).getChildAt(0)).setChecked(true);
                    mainStatus = "0";
                }
//                else if (holder.tableStatus.getText().equals(holder.itemView.getContext().getString(R.string.status_1_1))) {
//                    ((RadioButton) ((RadioGroup) dialog.findViewById(R.id.status_container)).getChildAt(1)).setChecked(true);
//                    mainStatus = "1";
//                }
                else if (holder.tableStatus.getText().equals(holder.itemView.getContext().getString(R.string.status_2_1))) {
                    ((RadioButton) ((RadioGroup) dialog.findViewById(R.id.status_container)).getChildAt(2)).setChecked(true);
                    mainStatus = "2";
                } else if (holder.tableStatus.getText().equals(holder.itemView.getContext().getString(R.string.status_3_1))) {
                    ((RadioButton) ((RadioGroup) dialog.findViewById(R.id.status_container)).getChildAt(3)).setChecked(true);
                    mainStatus = "3";
                } else {
                    ((RadioButton) ((RadioGroup) dialog.findViewById(R.id.status_container)).getChildAt(4)).setChecked(true);
                    mainStatus = "4";
                }
                ((TextView) dialog.findViewById(R.id.dialog_ok_btn)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            dialog.dismiss();
                            changeStatus(holder.itemView.getContext(), position, getStatus(holder.itemView.getContext(), dialog), mainStatus);
                        } catch (JSONException e) {
                            Log.e("Error parsing", e.toString());
                            e.printStackTrace();
                        }
                    }
                });

                ((TextView) dialog.findViewById(R.id.dialog_cancel_btn)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                switch (screenSize) {
                    case 0:
                        dialog.getWindow().setLayout((int) (1.5 * width) / 3, LinearLayout.LayoutParams.WRAP_CONTENT);
                        break;
                    case 1:
                        dialog.getWindow().setLayout((1 * width) / 4, LinearLayout.LayoutParams.WRAP_CONTENT);
                        break;
                    case 2:
                    default:
                        dialog.getWindow().setLayout((2 * width) / 3, LinearLayout.LayoutParams.WRAP_CONTENT);
                }
                dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                dialog.show();
            }
        });
    }

    private void changeStatus(final Context context, final int position, final String status, String mainStatus) throws JSONException {
        if (!status.equals(mainStatus)) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", tables.get(position).getId());
            jsonObject.put("user_id", tables.get(position).getUserId());
            jsonObject.put("branch_id", tables.get(position).getBranchId());
            jsonObject.put("chairs_count", tables.get(position).getChairsCount());
            jsonObject.put("reservation_time", tables.get(position).getReservationTime());
            jsonObject.put("status", status);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                    URLs.EDIT_RESERVATION,
                    jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("Edit Reserv response", response.toString());
//                            if (status.equals("0")) {
//                                tables.get(position).setStatus(context.getString(R.string.status_0_1));
//                            } else if (status.equals("1")) {
//                                tables.get(position).setStatus(context.getString(R.string.status_1_1));
//                            } else if (status.equals("2")) {
//                                tables.get(position).setStatus(context.getString(R.string.status_2_1));
//                            } else if (status.equals("3")) {
//                                tables.get(position).setStatus(context.getString(R.string.status_3_1));
//                            } else {
//                                tables.get(position).setStatus(context.getString(R.string.status_4_1));
//                            }
                            tables.remove(position);
                            setData(tables);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("Edit Reserv error", error.toString());
                            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                                Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Authorization", token);
                    return headers;
                }
            };
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MySingleton.getmInstance(context).addToRQ(jsonObjectRequest);
        }

    }

    private String getStatus(Context context, Dialog dialog) {
        int selected = ((RadioGroup) dialog.findViewById(R.id.status_container)).getCheckedRadioButtonId();
        RadioButton statusRBtn = (RadioButton) dialog.findViewById(selected);
        if (statusRBtn == null) {
            return null;
        }
        if (statusRBtn.getText().toString().trim().equals(context.getString(R.string.status_0_1))) {
            return "0";
        }
//        else if (statusRBtn.getText().toString().trim().equals(context.getString(R.string.status_1_1))) {
//            return "1";
//        }
        else if (statusRBtn.getText().toString().trim().equals(context.getString(R.string.status_2_1))) {
            return "2";
        } else if (statusRBtn.getText().toString().trim().equals(context.getString(R.string.status_3_1))) {
            return "3";
        } else {
            return "4";
        }
    }


    private void deleteReservation(final int position, final Context context) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URLs.DELETE_RESERVATION + "/" + tables.get(position).getId(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("reserv deletion response", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equals("done")) {
                                Toast.makeText(context, context.getString(R.string.success), Toast.LENGTH_LONG).show();
                                tables.remove(position);
                                notifyDataSetChanged();
                                Intent intent = new Intent(context, MainActivity.class);
                                intent.putExtra("GoToTables", "GoToTables");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                            } else {
                                Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("reserv deletion error", error.toString());
                        if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                            Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    @Override
    public int getItemCount() {
        return (null != tables ? tables.size() : 0);
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CircleImageView logo;
        ImageView delete;
        TextView name, chairsCount, reservationTime, tableStatus, reservationId;

        public Holder(View itemView) {
            super(itemView);
            logo = (CircleImageView) itemView.findViewById(R.id.table_brand_logo);
            delete = (ImageView) itemView.findViewById(R.id.delete_reservation);
            name = (TextView) itemView.findViewById(R.id.tables_branch_name);
            reservationId = (TextView) itemView.findViewById(R.id.tables_reservation_id);
            chairsCount = (TextView) itemView.findViewById(R.id.tables_chairs_count);
            reservationTime = (TextView) itemView.findViewById(R.id.tables_reservation_date);
            tableStatus = (TextView) itemView.findViewById(R.id.tables_status);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            listener.onListItemClickListener(position);
        }
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}
