package com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails;

import java.io.Serializable;

/**
 * Created by seif on 1/4/2018.
 */
public class ItemExtrasDetails implements Serializable {
    private int id;
    private String name;
    private double price;

    public ItemExtrasDetails() {
    }

    public ItemExtrasDetails(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}

