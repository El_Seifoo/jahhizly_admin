package com.example.seif.jahhizlyadmin2.Login;

import android.content.Context;
import android.content.Intent;

/**
 * Created by seif on 12/31/2017.
 */
public interface LoginMVP {
    interface view {
        String getEmail();

        String getPassword();

        void showMessage(String message);

        void showProgress();

        void hideProgress();

        void goHome();

        void clearPassword();

        boolean checkMailValidation(String email);

    }

    interface presenter {
        void onLoginCLicked(Context context, String email, String password);

        void onLoginCreated(Context context, Intent intent);
    }
}
