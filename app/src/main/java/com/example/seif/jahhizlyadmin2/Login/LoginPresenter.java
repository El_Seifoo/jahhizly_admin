package com.example.seif.jahhizlyadmin2.Login;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.seif.jahhizlyadmin2.Profile.UserInfo;
import com.example.seif.jahhizlyadmin2.R;
import com.example.seif.jahhizlyadmin2.Utils.MyAndroidFirebaseInstanceIdService;
import com.example.seif.jahhizlyadmin2.Utils.MySingleton;

/**
 * Created by seif on 12/31/2017.
 */
public class LoginPresenter implements LoginMVP.presenter, LoginModel.VolleyCallback {
    LoginMVP.view view;
    LoginModel model;

    public LoginPresenter(LoginMVP.view view, LoginModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void onLoginCLicked(Context context, String email, String password) {
        if (email.equals("") || password.equals("")) {
            view.showMessage(context.getString(R.string.error_1));
            view.clearPassword();
            return;
        }
        if (!view.checkMailValidation(email)) {
            view.showMessage(context.getString(R.string.check_mail));
            view.clearPassword();
            return;
        }
        ////////////////////////
        // Login Api at model //
        ////////////////////////
        view.showProgress();
        model.login(context, this, email, password);
    }

    @Override
    public void onLoginCreated(Context context, Intent intent) {
        if (intent.hasExtra("Logout")) {
            MySingleton.getmInstance(context).logout();
        } else if (MySingleton.getmInstance(context).isLoggedIn()) {
            view.goHome();
        }
    }

    @Override
    public void onSuccess(Context context, String response, int which) throws JSONException {
        if (which == 1) {
            Log.e("login response ", response.toString());
            JSONObject responseJson = new JSONObject(response);
            String token = responseJson.getString("token");
            MySingleton.getmInstance(context).saveUser(token);
            model.profile(context, this);
        } else if (which == 2) {
            JSONObject jsonObject = new JSONObject(response);
            JSONObject user = jsonObject.getJSONObject("user");
            JSONObject branch = jsonObject.getJSONObject("branch");

            int id = user.getInt("id");
            int adminId = user.getInt("admin_id");
            String email = user.getString("email");
            String phone = user.getString("phone");
            String userName = user.getString("name");
            String photo;
            if (user.getString("photo").equals("") ||
                    user.getString("photo").equals("null")) {
                photo = "";
            } else {
                photo = user.getString("photo");
            }
            String birthday;
            if (user.getString("birthday").equals("") ||
                    user.getString("birthday").equals("null")) {
                birthday = "";
            } else if (user.getString("birthday").contains("T")) {
                birthday = user.getString("birthday").split("T")[0];
            } else {
                birthday = user.getString("birthday");
            }
            // female 1... male 0
            String gender;
            if (user.getInt("gender") == 0) {
                gender = "Male";
            } else {
                gender = "Female";
            }

            int branchId = branch.getInt("id");
            String branchName = branch.getString("name");
            String branchArabicName = branch.getString("arabic_name");
            String branchPhone;
            if (branch.getString("phone").equals("") ||
                    branch.getString("phone").equals("null")) {
                branchPhone = "";
            } else {
                branchPhone = branch.getString("phone");
            }
            String tablePositionPhoto;
            if (branch.getString("tables_positions").equals("") ||
                    branch.getString("tables_positions").equals("null")) {
                tablePositionPhoto = "";
            } else {
                tablePositionPhoto = branch.getString("tables_positions");
            }
            int brandId = 0;
            String brandLogo = "";
            if (jsonObject.getJSONObject("brand") != null) {
                JSONObject brand = jsonObject.getJSONObject("brand");
                brandId = brand.getInt("id");
                if (brand.getString("photo").equals("") ||
                        brand.getString("photo").equals("null")) {
                    brandLogo = "";
                } else {
                    brandLogo = brand.getString("photo");
                }
            }

            MySingleton.getmInstance(context).saveUserData(new UserInfo(id, adminId, email, phone, photo, userName, birthday,
                    gender, branchId, branchName, branchArabicName, branchPhone, tablePositionPhoto, brandId, brandLogo));
            MySingleton.getmInstance(context).loginUser();
            MyAndroidFirebaseInstanceIdService seif = new MyAndroidFirebaseInstanceIdService(context);
            seif.onTokenRefresh();
            view.hideProgress();
            view.goHome();
        }
    }

    @Override
    public void onFail(Context context, VolleyError error) {
        view.hideProgress();
        if (error instanceof AuthFailureError) {
            view.showMessage(context.getString(R.string.auth_failure_error));
        } else if (error instanceof NoConnectionError || error instanceof TimeoutError) {
            view.showMessage(context.getString(R.string.internet_error));
        } else if (error instanceof ServerError) {
            view.showMessage(context.getString(R.string.server_error));
        } else {
            view.showMessage(context.getString(R.string.wrong));
        }

    }
}
