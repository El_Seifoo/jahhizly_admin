package com.example.seif.jahhizlyadmin2.Splash;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.seif.jahhizlyadmin2.Home.MainActivity;
import com.example.seif.jahhizlyadmin2.Login.LoginActivity;
import com.example.seif.jahhizlyadmin2.R;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
//        final MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.splash_sound);
//        mediaPlayer.start();
        new Thread() {
            public void run() {
                try {
                    sleep(4000);
                    loadMain();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    loadMain();
                }
            }
        }.start();
    }


    protected void loadMain() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
