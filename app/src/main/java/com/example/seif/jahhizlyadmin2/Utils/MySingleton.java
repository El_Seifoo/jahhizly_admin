package com.example.seif.jahhizlyadmin2.Utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import com.example.seif.jahhizlyadmin2.Profile.UserInfo;
import com.example.seif.jahhizlyadmin2.R;
import com.google.gson.Gson;

/**
 * Created by seif on 10/23/2017.
 */
public class MySingleton extends Application{
    private static MySingleton mInstance ;
    private RequestQueue requestQueue ;
    private static Context context ;
    private SharedPreferences sharedPreferences ;

    private MySingleton(Context context){
        this.context = context ;
        requestQueue = getRequestQueue() ;
        sharedPreferences = getSharedPreferences();
    }

    public RequestQueue getRequestQueue(){
        if(requestQueue == null ){
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return requestQueue;
    }

    public static synchronized MySingleton getmInstance(Context context){
        if(mInstance == null){
            mInstance = new MySingleton(context);
        }

        return mInstance;
    }


    public void addToRQ(Request request){
        requestQueue.add(request);
    }


    public SharedPreferences getSharedPreferences() {
        if (sharedPreferences == null){
            sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        }
        return sharedPreferences ;
    }

    public void saveUserData (UserInfo userInfo){
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        Gson gson = new Gson();
        String json = gson.toJson(userInfo);
        editor.putString(Constants.USER_DATA , json);
        editor.apply();
    }




    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    //--------------------- Language -------------------------\\
    public void setAppLang(String language){
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(Constants.APP_LANG , language);
        editor.apply();
    }

    public String getAppLang(){
        return getSharedPreferences().getString(Constants.APP_LANG , context.getString(R.string.settings_language_default));
    }
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////

    public void saveUser(String token){
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(Constants.USER_TOKEN , token);
        editor.apply();
    }


    public void loginUser() {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(Constants.IS_LOGGED_IN, true);
        editor.apply();
    }

    public boolean isLoggedIn(){
        return getSharedPreferences().getBoolean(Constants.IS_LOGGED_IN , false);
    }

    public String UserKey(){
        return sharedPreferences.getString(Constants.USER_TOKEN , "");
    }

    public UserInfo userData(){
        Gson gson = new Gson();
        String json = sharedPreferences.getString(Constants.USER_DATA , "");
        return gson.fromJson(json , UserInfo.class);
    }


    public void logout() {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.clear();
        editor.apply();
    }


}
