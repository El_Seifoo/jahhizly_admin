package com.example.seif.jahhizlyadmin2.Home.Orders;

import android.app.Dialog;
import android.content.Context;
import android.os.Vibrator;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.seif.jahhizlyadmin2.R;
import com.example.seif.jahhizlyadmin2.Utils.MySingleton;
import com.example.seif.jahhizlyadmin2.Utils.URLs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import me.anwarshahriar.calligrapher.Calligrapher;

/**
 * Created by seif on 1/4/2018.
 */
public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.Holder> {
    private ArrayList<Orders> orders;
    final private OrdersListItemClickListener listener;
    private int width, height;
    private int screenSize;
    private boolean flag;

    public OrdersAdapter(OrdersListItemClickListener listener, int width, int height, int screenSize, boolean flag) {
        this.listener = listener;
        this.width = width;
        this.height = height;
        this.screenSize = screenSize;
        this.flag = flag;
    }

    public interface OrdersListItemClickListener {
        void onListItemClickListener(int position);
    }

    public void setData(ArrayList<Orders> orders) {
        this.orders = orders;
        notifyDataSetChanged();
    }

    @Override
    public OrdersAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.orders_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final OrdersAdapter.Holder holder, final int position) {
        Calligrapher calligrapher = new Calligrapher(holder.itemView.getContext());
        if (MySingleton.getmInstance(holder.itemView.getContext()).getAppLang().equals(holder.itemView.getContext().getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(holder.itemView, "fonts/Arabic/arabic_font.ttf");
        } else {
            calligrapher.setFont(holder.itemView, "fonts/English/Roboto_Regular.ttf");
        }
        Glide.with(holder.itemView.getContext()).load(URLs.ROOT_URL + orders.get(position).getPhoto())
                .error(R.mipmap.restaurant_meal_icon)
                .crossFade()
                .thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.brandLogo);

        holder.branchName.setText(flag ? convertDigits(orders.get(position).getBranchName()) : orders.get(position).getBranchName());
        holder.orderId.setText(flag ? convertDigits(holder.itemView.getContext().getString(R.string.order_num) + orders.get(position).getId() + "  ") : holder.itemView.getContext().getString(R.string.order_num) + orders.get(position).getId() + "  ");
        holder.orderStatus.setText(orders.get(position).getStatus());
        holder.itemPrice.setText(flag ? convertDigits(" " + orders.get(position).getPrice() + holder.itemView.getContext().getString(R.string.currency) + "  ") : " " + orders.get(position).getPrice() + holder.itemView.getContext().getString(R.string.currency) + "  ");
        String[] time = orders.get(position).getDate().split("-");
        holder.orderDate.setText(flag ? convertDigits(" " + time[2] + " / " + time[1] + " / " + time[0] + "  ") : " " + time[0] + " / " + time[1] + " / " + time[2] + "  ");
        holder.itemTime.setText(flag ? convertDigits(orders.get(position).getPreparationTime() + "") : orders.get(position).getPreparationTime() + "");
        if (orders.get(position).getStatus().equals(holder.itemView.getContext().getString(R.string.status_3)) || orders.get(position).getStatus().equals(holder.itemView.getContext().getString(R.string.status_4))) {
            holder.itemTime.setVisibility(View.GONE);
        }
        holder.orderStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(holder.itemView.getContext());
                dialog.setContentView(R.layout.status_dialog);
                final String mainStatus;
                if (holder.orderStatus.getText().equals(holder.itemView.getContext().getString(R.string.status_0))) {
                    ((RadioButton) ((RadioGroup) dialog.findViewById(R.id.status_container)).getChildAt(0)).setChecked(true);
                    mainStatus = "0";
                } else if (holder.orderStatus.getText().equals(holder.itemView.getContext().getString(R.string.status_1))) {
                    ((RadioButton) ((RadioGroup) dialog.findViewById(R.id.status_container)).getChildAt(1)).setChecked(true);
                    mainStatus = "1";
                } else if (holder.orderStatus.getText().equals(holder.itemView.getContext().getString(R.string.status_2))) {
                    ((RadioButton) ((RadioGroup) dialog.findViewById(R.id.status_container)).getChildAt(2)).setChecked(true);
                    mainStatus = "2";
                } else if (holder.orderStatus.getText().equals(holder.itemView.getContext().getString(R.string.status_3))) {
                    ((RadioButton) ((RadioGroup) dialog.findViewById(R.id.status_container)).getChildAt(3)).setChecked(true);
                    mainStatus = "3";
                } else {
                    ((RadioButton) ((RadioGroup) dialog.findViewById(R.id.status_container)).getChildAt(4)).setChecked(true);
                    mainStatus = "4";
                }
                ((TextView) dialog.findViewById(R.id.dialog_ok_btn)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            dialog.dismiss();
                            changeStatus(holder.itemView.getContext(), position, getStatus(holder.itemView.getContext(), dialog), mainStatus);
                        } catch (JSONException e) {
                            Log.e("Error parsing", e.toString());
                            e.printStackTrace();
                        }
                    }
                });

                ((TextView) dialog.findViewById(R.id.dialog_cancel_btn)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dialog.dismiss();
                    }
                });
                switch (screenSize) {
                    case 0:
                        dialog.getWindow().setLayout((int) (1.5 * width) / 3, LinearLayout.LayoutParams.WRAP_CONTENT);
                        break;
                    case 1:
                        dialog.getWindow().setLayout((1 * width) / 4, LinearLayout.LayoutParams.WRAP_CONTENT);
                        break;
                    case 2:
                    default:
                        dialog.getWindow().setLayout((2 * width) / 3, LinearLayout.LayoutParams.WRAP_CONTENT);
                }

                dialog.show();
            }
        });

    }

    private String getStatus(Context context, Dialog dialog) {
        int selected = ((RadioGroup) dialog.findViewById(R.id.status_container)).getCheckedRadioButtonId();
        RadioButton statusRBtn = (RadioButton) dialog.findViewById(selected);
        if (statusRBtn == null) {
            return null;
        }
        if (statusRBtn.getText().toString().trim().equals(context.getString(R.string.status_0))) {
            return "0";
        } else if (statusRBtn.getText().toString().trim().equals(context.getString(R.string.status_1))) {
            return "1";
        } else if (statusRBtn.getText().toString().trim().equals(context.getString(R.string.status_2))) {
            return "2";
        } else if (statusRBtn.getText().toString().trim().equals(context.getString(R.string.status_3))) {
            return "3";
        } else {
            return "4";
        }
    }

    private void changeStatus(final Context context, final int position, final String status, String mainStatus) throws JSONException {
        if (!status.equals(mainStatus)) {
            JSONObject order = new JSONObject();
            order.put("id", orders.get(position).getId());
            order.put("user_id", orders.get(position).getUserId());
            order.put("branch_id", orders.get(position).getBranchId());
            order.put("order_time", orders.get(position).getPreparationTime());
            order.put("status", status);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                    URLs.EDIT_ORDER,
                    order,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("Edit Order response", response.toString());
                            try {
                                if (response.getString("status").equals("success")) {
//                                    if (status.equals("0")) {
//                                        orders.get(position).setStatus(context.getString(R.string.status_0));
//                                    } else if (status.equals("1")) {
//                                        orders.get(position).setStatus(context.getString(R.string.status_1));
//                                    } else if (status.equals("2")) {
//                                        orders.get(position).setStatus(context.getString(R.string.status_2));
//                                    } else if (status.equals("3")) {
//                                        orders.get(position).setStatus(context.getString(R.string.status_3));
//                                    } else {
//                                        orders.get(position).setStatus(context.getString(R.string.status_4));
//                                    }
                                    orders.remove(position);
                                    setData(orders);
                                } else {
                                    Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("Edit Order error", error.toString());
                            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                                Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Authorization", token);
                    return headers;
                }
            };
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MySingleton.getmInstance(context).addToRQ(jsonObjectRequest);
        }
    }


    @Override
    public int getItemCount() {
        return (null != orders ? orders.size() : 0);
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CircleImageView brandLogo;
        TextView branchName, orderDate, itemTime, itemPrice, orderId, orderStatus;

        public Holder(View itemView) {
            super(itemView);
            brandLogo = (CircleImageView) itemView.findViewById(R.id.order_brand_logo);
            branchName = (TextView) itemView.findViewById(R.id.orders_branch_name);
            orderId = (TextView) itemView.findViewById(R.id.orders_num);
            orderDate = (TextView) itemView.findViewById(R.id.orders_date);
            itemTime = (TextView) itemView.findViewById(R.id.orders_item_time);
            itemPrice = (TextView) itemView.findViewById(R.id.orders_item_price);
            orderStatus = (TextView) itemView.findViewById(R.id.order_status);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            listener.onListItemClickListener(position);
        }
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}
