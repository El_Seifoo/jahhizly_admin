package com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails;

import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.seif.jahhizlyadmin2.Home.MainActivity;
import com.example.seif.jahhizlyadmin2.R;
import com.example.seif.jahhizlyadmin2.Utils.MySingleton;
import com.example.seif.jahhizlyadmin2.Utils.URLs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import me.anwarshahriar.calligrapher.Calligrapher;

/**
 * Created by seif on 1/4/2018.
 */
public class OrderDetailsAdapter extends RecyclerView.Adapter<OrderDetailsAdapter.Holder> {

    private ArrayList<OrdersDetails> orders = new ArrayList<>();
    final private OrderDetailsListItemClickListener listener;
    private boolean flag;

    public OrderDetailsAdapter(OrderDetailsListItemClickListener listener, boolean flag) {
        this.listener = listener;
        this.flag = flag;
    }

    public void setOrdersDetails(ArrayList<OrdersDetails> orders) {
        this.orders = orders;
    }

    public interface OrderDetailsListItemClickListener {
        void onListItemClickListener(int position);
    }


    @Override
    public OrderDetailsAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_details_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final OrderDetailsAdapter.Holder holder, final int position) {
        Glide.with(holder.itemView.getContext()).load(URLs.ROOT_URL + orders.get(position).getPhoto())
                .error(R.mipmap.restaurant_meal_icon)
                .thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.orderItemLogo);
        holder.orderItemName.setText(orders.get(position).getItemName());
        String txt = holder.itemView.getContext().getString(R.string.sizes) + "<font color = '#eccd33'> " + orders.get(position).getSize() + " </font>";
        holder.orderItemSize.setText(Html.fromHtml(flag ? convertDigits(txt) : txt), TextView.BufferType.SPANNABLE);
        holder.orderItemPrice.setText(flag ? convertDigits(orders.get(position).getPrice() + holder.itemView.getContext().getString(R.string.currency)) : orders.get(position).getPrice() + holder.itemView.getContext().getString(R.string.currency));
        String quantity = holder.itemView.getContext().getString(R.string.quantity) + "<font color = '#eccd33'> " + orders.get(position).getQuantity() + " </font>";
        holder.orderQuantity.setText(Html.fromHtml(flag ? convertDigits(quantity) : quantity), TextView.BufferType.SPANNABLE);
        if (orders.get(position).getExtraName().contains(",")) {
            String[] extras = orders.get(position).getExtraName().split(",");
            String text1 = holder.itemView.getContext().getString(R.string.extras);
            text1 += "<br>";
            String text = "";
            for (int i = 0; i < extras.length; i++) {
                text += extras[i] + " " + orders.get(position).getQuantity();
                text += "<br>";
            }
            String finalTxt = text1 + "<font color = '#eccd33'> " + text + " </font>";
            Log.e("textaya", finalTxt);
            holder.orderExtrasPrice.setText(Html.fromHtml(flag ? convertDigits(finalTxt) : finalTxt), TextView.BufferType.SPANNABLE);
        } else {

            if (orders.get(position).getExtraName().trim().equals("")) {
                holder.orderExtrasPrice.setVisibility(View.GONE);
            } else {
                String text = holder.itemView.getContext().getString(R.string.extras) + "<br>" + "<font color = '#eccd33'> " + orders.get(position).getExtraName() + " " + orders.get(position).getQuantity() + " </font>";
                holder.orderExtrasPrice.setText(Html.fromHtml(flag ? convertDigits(text) : text), TextView.BufferType.SPANNABLE);
            }
        }

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (orders.size() == 1) {
                    if (holder.itemView.getContext() instanceof OrderDetailsActivity) {
                        ((OrderDetailsActivity) holder.itemView.getContext()).deleteOrder();
                    }
                } else {
                    deleteItem(position, holder.itemView.getContext());
                }
            }
        });
    }

    private void deleteItem(final int position, final Context context) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URLs.DELETE_ITEM + "/" + orders.get(position).getId(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("delete item response", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equals("done")) {
                                Toast.makeText(context, context.getString(R.string.success), Toast.LENGTH_LONG).show();
                                orders.remove(position);
                                if (context instanceof OrderDetailsActivity) {
                                    if (orders.isEmpty()) {
                                        ((OrderDetailsActivity) context).showEmptyText();
                                    } else {
                                        ((OrderDetailsActivity) context).loadData(orders);
                                    }
                                }
                            } else {
                                Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();

                            }
                        } catch (JSONException e) {
                            Log.e("delete item parsing error", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("delete item error", error.toString());
                if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                    Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    @Override
    public int getItemCount() {
        return (null != orders ? orders.size() : 0);
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView orderItemLogo, delete;
        TextView orderItemName, orderItemSize, orderItemPrice, orderQuantity, orderExtrasPrice;

        public Holder(View itemView) {
            super(itemView);
            orderItemLogo = (ImageView) itemView.findViewById(R.id.prev_order_details_brand_logo);
            delete = (ImageView) itemView.findViewById(R.id.delete_item);
            orderItemName = (TextView) itemView.findViewById(R.id.prev_order_details_item_name);
            orderItemSize = (TextView) itemView.findViewById(R.id.prev_order_details_item_size);
            orderItemPrice = (TextView) itemView.findViewById(R.id.prev_order_details_item_price);
            orderQuantity = (TextView) itemView.findViewById(R.id.prev_order_details_item_quantity);
            orderExtrasPrice = (TextView) itemView.findViewById(R.id.prev_order_details_item_extras);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            listener.onListItemClickListener(position);
        }
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}

