package com.example.seif.jahhizlyadmin2.Home;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.seif.jahhizlyadmin2.Utils.MySingleton;
import com.example.seif.jahhizlyadmin2.Utils.URLs;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by seif on 1/4/2018.
 */
public class HomeModel {
    public HomeModel() {
    }

    protected void getOrders(final Context context, final VolleyCallback callback, final int page, String status, String minPrice, String maxPrice, String key) {
//                       /orders/   {currentPage}/   {brand}/      {branch}/     {status}/      {gender}/    {location}/    {min_price}/     {max_price}
        String url = URLs.ORDERS + "/" + page + "/" + "all" + "/" + "all" + "/" + status.toLowerCase().trim() + "/" + "all" + "/" + "all" + "/" + minPrice + "/" + maxPrice;
        if (key != null && !key.equals("")) {
            try {
                url += "/" + URLEncoder.encode(key, "utf-8");
            } catch (UnsupportedEncodingException e) {
                Log.e("url", e.toString());
                e.printStackTrace();
            }
        }
        Log.e("Orders url", url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Orders Response", response);
                        try {
                            callback.onSuccess(context, response, page);
                        } catch (JSONException e) {
                            Log.e("parsing orders error ", e.toString());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFail(context, error, page);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }
    protected void getOrders(final Context context, final VolleyCallback callback, String status, String minPrice, String maxPrice, String key) {
//                       /orders/   {currentPage}/   {brand}/      {branch}/     {status}/      {gender}/    {location}/    {min_price}/     {max_price}
        String url = URLs.ORDERS + "/" + "0" + "/" + "all" + "/" + "all" + "/" + status.toLowerCase().trim() + "/" + "all" + "/" + "all" + "/" + minPrice + "/" + maxPrice;
        if (key != null && !key.equals("")) {
            try {
                url += "/" + URLEncoder.encode(key, "utf-8");
            } catch (UnsupportedEncodingException e) {
                Log.e("url", e.toString());
                e.printStackTrace();
            }
        }
        Log.e("Orders url 1", url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Orders Response 1", response);
                        try {
                            callback.onSuccess(context, response, -1);
                        } catch (JSONException e) {
                            Log.e("parsing orders error ", e.toString());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFail(context, error, -1);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected interface VolleyCallback {
        void onSuccess(Context context, String response, int page) throws JSONException;

        void onFail(Context context, VolleyError error, int page);
    }
}
