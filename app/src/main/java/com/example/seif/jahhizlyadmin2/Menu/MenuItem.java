package com.example.seif.jahhizlyadmin2.Menu;

import com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails.ItemExtrasDetails;
import com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails.ItemSizeDetails;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by seif on 1/7/2018.
 */
public class MenuItem implements Serializable {
    private int id;
    private int branchId;
    private String name;
    private String arabicName;
    private String recipe;
    private String photo;
    private String time;
    private boolean status;
    private int menuCatId;
    private boolean isChecked;
    private double price;
    private ArrayList<ItemSizeDetails> itemSizeDetails;
    private ArrayList<ItemExtrasDetails> itemExtrasDetails;

    public MenuItem(int id, int branchId, String name, String arabicName, String recipe, String photo, String time, boolean status, int menuCatId, boolean isChecked, double price) {
        this.id = id;
        this.branchId = branchId;
        this.name = name;
        this.arabicName = arabicName;
        this.recipe = recipe;
        this.photo = photo;
        this.time = time;
        this.status = status;
        this.menuCatId = menuCatId;
        this.isChecked = isChecked;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public String getRecipe() {
        return recipe;
    }

    public void setRecipe(String recipe) {
        this.recipe = recipe;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getMenuCatId() {
        return menuCatId;
    }

    public void setMenuCatId(int menuCatId) {
        this.menuCatId = menuCatId;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ArrayList<ItemSizeDetails> getItemSizeDetails() {
        return itemSizeDetails;
    }

    public void setItemSizeDetails(ArrayList<ItemSizeDetails> itemSizeDetails) {
        this.itemSizeDetails = itemSizeDetails;
    }

    public ArrayList<ItemExtrasDetails> getItemExtrasDetails() {
        return itemExtrasDetails;
    }

    public void setItemExtrasDetails(ArrayList<ItemExtrasDetails> itemExtrasDetails) {
        this.itemExtrasDetails = itemExtrasDetails;
    }
}
