package com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails;

import android.app.Dialog;
import android.content.Context;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by seif on 1/4/2018.
 */
public interface OrderMVP {
    interface view {
        void loadData(ArrayList<OrdersDetails> orders);

        void showProgress();

        void hideProgress();

        void showEmptyText();

        void showMessage(String message);

        //////////////////////////////
        void showDialogProgress();

        void hideDialogProgress();

        void showItemDialog(Dialog dialog, int menuId, int index);

        void increase();

        void decrease();

        void sizesList(Dialog dialog, ArrayList<ItemSizeDetails> itemSizeDetails, int index);

        void checkBoxExtra(Dialog dialog, ArrayList<ItemExtrasDetails> itemExtrasDetails, int index);

        void editListItem(Dialog dialog, ArrayList<OrdersDetails> list, int index, int sizeId, String size, String extrasId, String extras, int quantity, double price, double extrasPrices);

        int getSizeId();

        String getSize();

        String getExtrasId();

        String getExtras();

        double getPrice();

        int getQuantity();

        void deletionDone(String message);
    }

    interface presenter {
        void requestOrderDetails(Context context, int orderId);

        void onListItemClicked(Context context, Dialog dialog, int menuId, int index);

        void requestSizesExtras(Context context, Dialog dialog, int menuId, int index);

        void onPlusClicked();

        void onMinusClicked();

        void onEditBtnClicked(Dialog dialog, ArrayList<OrdersDetails> list, int index, int sizeId, String size, String extrasId, String extras, int quantity, double price, double extrasPrice);

        void saveEditedData(Context context, int orderId, int userId, int branchId, int orderTime, ArrayList<OrdersDetails> orderList, int status);

        void deleteOrderClicked(Context context, int orderId);
    }
}
