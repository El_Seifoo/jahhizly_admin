package com.example.seif.jahhizlyadmin2.Profile;

/**
 * Created by seif on 1/10/2018.
 */
public class UserInfo {
    private int id;
    private int adminId;
    private String email;
    private String phone;
    private String photo;
    private String name;
    private String birthday;
    private String gender;
    /////////////////////
    private int branchId;
    private String branchName;
    private String branchArabicName;
    private String branchPhone;
    private String tablePositionPhoto;
    ///////////////////
    private int brandId;
    private String brandLogo;

    public UserInfo(int id, int adminId, String email, String phone, String photo, String name,
                    String birthday, String gender, int branchId, String branchName,
                    String branchArabicName, String branchPhone, String tablePositionPhoto, int brandId, String brandLogo) {
        this.id = id;
        this.adminId = adminId;
        this.email = email;
        this.phone = phone;
        this.photo = photo;
        this.name = name;
        this.birthday = birthday;
        this.gender = gender;
        this.branchId = branchId;
        this.branchName = branchName;
        this.branchArabicName = branchArabicName;
        this.branchPhone = branchPhone;
        this.tablePositionPhoto = tablePositionPhoto;
        this.brandId = brandId;
        this.brandLogo = brandLogo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAdminId() {
        return adminId;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchArabicName() {
        return branchArabicName;
    }

    public void setBranchArabicName(String branchArabicName) {
        this.branchArabicName = branchArabicName;
    }

    public String getBranchPhone() {
        return branchPhone;
    }

    public void setBranchPhone(String branchPhone) {
        this.branchPhone = branchPhone;
    }

    public String getTablePositionPhoto() {
        return tablePositionPhoto;
    }

    public void setTablePositionPhoto(String tablePositionPhoto) {
        this.tablePositionPhoto = tablePositionPhoto;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public String getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(String brandLogo) {
        this.brandLogo = brandLogo;
    }
}
