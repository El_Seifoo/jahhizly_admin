package com.example.seif.jahhizlyadmin2.Home;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.example.seif.jahhizlyadmin2.Home.Orders.Orders;
import com.example.seif.jahhizlyadmin2.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by seif on 1/4/2018.
 */
public class HomePresenter implements HomeMVP.OrdersPresenter, HomeModel.VolleyCallback {

    private HomeModel ordersModel;
    private HomeMVP.OrdersView ordersView;
    boolean flag;

    public HomePresenter(HomeMVP.OrdersView ordersView, HomeModel ordersModel, boolean flag) {
        this.ordersModel = ordersModel;
        this.ordersView = ordersView;
        this.flag = flag;
    }

    @Override
    public void requestOrders(Context context, int page, String status, String minPrice, String maxPrice, String key) {
        if (page > 0) {
            ordersView.showLoadingMoreProgress();
            ordersModel.getOrders(context, this, page, status, minPrice, maxPrice, key);
        } else {
            ordersView.showProgress();
            ordersModel.getOrders(context, this, page, status, minPrice, maxPrice, key);
        }
    }

    @Override
    public void requestOrders(Context context, String status, String minPrice, String maxPrice, String key) {
        ordersModel.getOrders(context, this, status, minPrice, maxPrice, key);
    }


    @Override
    public void onFabClicked() {
        ordersView.fabAction();
    }

    @Override
    public void onSuccess(Context context, String response, int page) throws JSONException {
        if (page > 0) {
            ordersView.hideLoadingMoreProgress();
        } else {
            ordersView.hideProgress();
        }
        ArrayList<Orders> retrievedOrders = new ArrayList<>();
        JSONObject jsonResponse = new JSONObject(response);
        JSONArray orders = jsonResponse.getJSONArray("orders");
        for (int i = 0; i < orders.length(); i++) {
            int id = orders.getJSONObject(i).getInt("id");
            int userId = orders.getJSONObject(i).getInt("user_id");
            int branchId = orders.getJSONObject(i).getInt("branch_id");
            int reservationId;
            if (orders.getJSONObject(i).getString("reservation_id").equals("") ||
                    orders.getJSONObject(i).getString("reservation_id").equals("null")) {
                reservationId = 0;
            } else {
                reservationId = orders.getJSONObject(i).getInt("reservation_id");
            }
            int statusInt = orders.getJSONObject(i).getInt("status");
            String status = "";
            // pending , inProgress , ready, delivered , canceled
            if (statusInt == 0) {
                status = context.getString(R.string.status_0);
            } else if (statusInt == 1) {
                status = context.getString(R.string.status_1);
            } else if (statusInt == 2) {
                status = context.getString(R.string.status_2);
            } else if (statusInt == 3) {
                status = context.getString(R.string.status_3);
            } else if (statusInt == 4) {
                status = context.getString(R.string.status_4);
            }
            String[] createdAt = orders.getJSONObject(i).getString("created_at").split(" ");
            String date = createdAt[0];
            String time = createdAt[1].split(":")[0] + ":" + createdAt[1].split(":")[1];
            double price;
            if (orders.getJSONObject(i).getString("price").equals("") ||
                    orders.getJSONObject(i).getString("price").equals("null")) {
                price = 0;
            } else {
                price = orders.getJSONObject(i).getDouble("price");
            }
            int prepTime;
            if (orders.getJSONObject(i).getString("order_time").equals("") ||
                    orders.getJSONObject(i).getString("order_time").equals("null")) {
                prepTime = 0;
            } else {
                prepTime = orders.getJSONObject(i).getInt("order_time");
            }
            if (flag){

            }else {

            }
            retrievedOrders.add(new Orders(id, userId, branchId, reservationId, status, price, date, time, prepTime, "null"));
        }
        if (retrievedOrders.size() > 0) {
            ordersView.loadData(retrievedOrders);
        } else {
            if (page == 0) {
                ordersView.showEmptyTxt();
            }
        }
    }

    @Override
    public void onFail(Context context, VolleyError error, int page) {
        if (page != -1) {
            Log.e("Orders Error", error.toString());
            if (page > 0) {
                ordersView.hideLoadingMoreProgress();
            } else {
                ordersView.hideProgress();
            }
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                ordersView.showMessage(context.getString(R.string.internet_error));
            } else if (error instanceof ServerError) {
                ordersView.showMessage(context.getString(R.string.server_error));
            } else {
                ordersView.showMessage(context.getString(R.string.wrong));

            }
        }
    }
}
