package com.example.seif.jahhizlyadmin2.Tables;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.seif.jahhizlyadmin2.Home.Orders.OrdersAdapter;
import com.example.seif.jahhizlyadmin2.R;
import com.example.seif.jahhizlyadmin2.Tables.TableDetails.TableDetails;
import com.example.seif.jahhizlyadmin2.Utils.MySingleton;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

/**
 * Created by seif on 1/11/2018.
 */
public class TablesFragment extends Fragment implements TablesMVP.view, TablesAdapter.TablesListItemClickListener {

    TablesPresenter presenter;
    MKLoader loading, loadingMore;
    int page = 0;
    RecyclerView tablesRecyclerView;
    TablesAdapter adapter;
    TextView emptyTextView;
    ArrayList<ReservedTables> allTables = new ArrayList<>();
    private Timer timer;

    public static TablesFragment newInstance(String status, String key) {
        TablesFragment fragment = new TablesFragment();
        Bundle args = new Bundle();
        args.putString("Status", status);
        args.putString("Key", (key != null) ? key : "");
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tables, container, false);
        design(view);
        presenter = new TablesPresenter(new TablesModel(), this);
        loading = (MKLoader) view.findViewById(R.id.loading);
        loadingMore = (MKLoader) view.findViewById(R.id.loading_more);
        tablesRecyclerView = (RecyclerView) view.findViewById(R.id.tables_recycler_view);
        emptyTextView = (TextView) view.findViewById(R.id.empty_list_text);
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                tablesRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false));
                adapter = new TablesAdapter(getActivity(), this, metrics.widthPixels, 0, MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                tablesRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false));
                adapter = new TablesAdapter(getActivity(), this, metrics.widthPixels, 1, MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                adapter = new TablesAdapter(getActivity(), this, metrics.widthPixels, 2, MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
                tablesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                break;
            default:
                adapter = new TablesAdapter(getActivity(), this, metrics.widthPixels, 3, MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
                tablesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        }

        tablesRecyclerView.setHasFixedSize(true);
        presenter.onTablesCreated(getContext(), page, getArguments().getString("Status"), convertDigits(getArguments().getString("Key")));
        tablesRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int pastVisiblesItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();

                if (dy > 0 && pastVisiblesItems >= adapter.getItemCount() - 1) {
                    page++;
                    presenter.onTablesCreated(getContext(), page, getArguments().getString("Status"), convertDigits(getArguments().getString("Key")));
                }
            }
        });

        return view;
    }

    private float getScreenWidth() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        float scaleFactor = displayMetrics.density;
        return (width / scaleFactor);
    }

    private void design(View view) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(params);
    }

    @Override
    public void onResume() {
        super.onResume();
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
                                      @Override
                                      public void run() {
                                          if (getContext() != null && page == 0) {
                                              Log.e("llllllll", "lllllllll");
                                              presenter.onTablesCreated(getContext(), getArguments().getString("Status"), convertDigits(getArguments().getString("Key")));
                                          }
                                      }
                                  }

                , 10000, 50000);
    }

    @Override
    public void showProgress() {
        Log.e("seif 1", "progress");
        emptyTextView.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        loading.setVisibility(View.GONE);

    }

    @Override
    public void showLoadingMoreProgress() {
        emptyTextView.setVisibility(View.GONE);
        loadingMore.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideLoadingMoreProgress() {

        loadingMore.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String message) {
        if (getContext() != null) {
            Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void showEmptyTxt() {
        Log.e("seif 2", "empty");
        emptyTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void loadData(ArrayList<ReservedTables> tables) {
        if (getContext() != null) {
            Log.e("seif 3", "empty");
            emptyTextView.setVisibility(View.GONE);
            if (MySingleton.getmInstance(getContext()).isLoggedIn()) {
                for (int i = 0; i < tables.size(); i++) {
                    tables.get(i).setPhoto(MySingleton.getmInstance(getContext()).userData().getBrandLogo());
                    if (MySingleton.getmInstance(getContext()).getAppLang().equals(getContext().getString(R.string.settings_language_arabic_value))) {
                        tables.get(i).setBranchName(MySingleton.getmInstance(getContext()).userData().getBranchArabicName());
                    } else {
                        tables.get(i).setBranchName(MySingleton.getmInstance(getContext()).userData().getBranchName());
                    }
                }
            }

            if (page == 0 && !areIdentical(tables, allTables)) {
                allTables = tables;
                int pastVisiblesItems = ((LinearLayoutManager) tablesRecyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
                tablesRecyclerView.scrollToPosition(pastVisiblesItems - 3);
                adapter.setData(tables);
                ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
                scaleInAnimationAdapter.setFirstOnly(false);
                tablesRecyclerView.setAdapter(scaleInAnimationAdapter);
            } else if (page == 0 && areIdentical(tables, allTables)) {
                // Do nothing
            } else if (page != 0) {
                for (int i = 0; i < tables.size(); i++) {
                    allTables.add(tables.get(i));
                }
                int pastVisiblesItems = ((LinearLayoutManager) tablesRecyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
                tablesRecyclerView.scrollToPosition(pastVisiblesItems - 3);
                adapter.setData(allTables);
                ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
                scaleInAnimationAdapter.setFirstOnly(false);
                tablesRecyclerView.setAdapter(scaleInAnimationAdapter);
            }
        }
    }

    private boolean areIdentical(ArrayList<ReservedTables> tables1, ArrayList<ReservedTables> tables2) {

        if (tables1 == null && tables2 == null)
            return true;
        if ((tables1 == null && tables2 != null) || (tables1 == null && tables1 != null))
            return false;

        if (tables1.size() != tables2.size())
            return false;


        for (int i = 0; i < tables1.size(); i++) {
            if (!(tables1.get(i).getId() == tables2.get(i).getId()))
                return false;
        }
        return true;
    }


    @Override
    public void onListItemClickListener(int position) {
        Intent intent = new Intent(getContext(), TableDetails.class);
        intent.putExtra("ReservationData", allTables.get(position));
        startActivity(intent);
    }


    @Override
    public void onPause() {
        super.onPause();
        if (timer != null) {
            timer.cancel();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (timer != null) {
            timer.cancel();
        }
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }


}
