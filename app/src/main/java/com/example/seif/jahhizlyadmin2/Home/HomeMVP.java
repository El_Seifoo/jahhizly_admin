package com.example.seif.jahhizlyadmin2.Home;

import android.content.Context;

import com.example.seif.jahhizlyadmin2.Home.Orders.Orders;

import java.util.ArrayList;

/**
 * Created by seif on 1/4/2018.
 */
public interface HomeMVP {
    interface OrdersView {
        void fabAction();

        void showProgress();

        void hideProgress();

        void showLoadingMoreProgress();

        void hideLoadingMoreProgress();

        void showMessage(String message);

        void loadData(ArrayList<Orders> orders);

        void showEmptyTxt();
    }

    interface OrdersPresenter {
        //        /orders/{currentPage}/{status}/{min_price}/{max_price}[/{key}]
        void requestOrders(Context context, int page, String status, String minPrice, String maxPrice, String key);
        void requestOrders(Context context, String status, String minPrice, String maxPrice, String key);

        void onFabClicked();
    }
}
