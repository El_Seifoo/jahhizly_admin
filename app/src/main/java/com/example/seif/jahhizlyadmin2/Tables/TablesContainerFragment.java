package com.example.seif.jahhizlyadmin2.Tables;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.eftimoff.viewpagertransformers.RotateDownTransformer;
import com.example.seif.jahhizlyadmin2.Home.Orders.OrdersFragment;
import com.example.seif.jahhizlyadmin2.R;

/**
 * Created by seif on 1/11/2018.
 */
public class TablesContainerFragment extends Fragment {
    private ViewPager viewPager = null;


    public static TablesContainerFragment newInstance(String key) {
        TablesContainerFragment fragment = new TablesContainerFragment();
        Bundle args = new Bundle();
        args.putString("Key", (key != null) ? key : "");
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tables_container, container, false);
        design(view);
        viewPager = (ViewPager) view.findViewById(R.id.pager);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        viewPager.setAdapter(new MyAdapter(fragmentManager, getArguments().getString("Key")));
        viewPager.setPageTransformer(true, new RotateDownTransformer());
        return view;
    }

    private void design(View view) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(params);
    }

    class MyAdapter extends FragmentStatePagerAdapter {
        private String key;

        @Override
        public Parcelable saveState() {
            return null;
        }

        public MyAdapter(FragmentManager fm, String key) {
            super(fm);
            this.key = key;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0) {
                fragment = TablesFragment.newInstance("all",key);
            } else if (position == 1) {
                fragment = TablesFragment.newInstance("0",key);
            }
//            else if (position == 2) {
//                fragment = TablesFragment.newInstance("1",key);
//            }
            else if (position == 2) {
                fragment = TablesFragment.newInstance("2",key);
            } else if (position == 3) {
                fragment = TablesFragment.newInstance("3",key);
            } else if (position == 4) {
                fragment = TablesFragment.newInstance("4",key);
            }
            return fragment;


        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return getString(R.string.all);
            } else if (position == 1) {
                return getString(R.string.status_0_1);
            }
//            else if (position == 2) {
//                return getString(R.string.status_1_1);
//            }
            else if (position == 2) {
                return getString(R.string.status_2_1);
            } else if (position == 3) {
                return getString(R.string.status_3_1);
            } else if (position == 4) {
                return getString(R.string.status_4_1);
            }
            return null;
        }
    }


}

