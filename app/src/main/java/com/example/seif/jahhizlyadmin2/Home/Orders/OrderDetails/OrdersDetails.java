package com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails;

import java.io.Serializable;

/**
 * Created by seif on 1/4/2018.
 */
public class OrdersDetails implements Serializable {
    private int quantity;
    private double price;
    private String itemName;
    private String extraId;
    private String extraName;
    private String extraPrice;
    private int sizeId;
    private String size;
    private int id;
    private int orderId;
    private int menuId;
    private int brandId;
    private String photo;

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public OrdersDetails(int quantity, double price, String itemName, String extraId, String extraName,
                         String extraPrice, int sizeId, String size, int id, int orderId, int menuId, int brandId, String photo) {
        this.quantity = quantity;
        this.price = price;
        this.itemName = itemName;
        this.extraId = extraId;
        this.extraName = extraName;
        this.extraPrice = extraPrice;
        this.sizeId = sizeId;
        this.size = size;
        this.id = id;
        this.orderId = orderId;
        this.menuId = menuId;
        this.brandId = brandId;
        this.photo = photo;
    }

//    quantity , price , name , extraid , extras, extraprice , sizeid , size ,menuid


    public OrdersDetails(int quantity, double price, String itemName, String extraId, String extraName, String extraPrice, int sizeId, String size, int menuId) {
        this.quantity = quantity;
        this.price = price;
        this.itemName = itemName;
        this.extraId = extraId;
        this.extraName = extraName;
        this.extraPrice = extraPrice;
        this.sizeId = sizeId;
        this.size = size;
        this.menuId = menuId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getExtraId() {
        return extraId;
    }

    public void setExtraId(String extraId) {
        this.extraId = extraId;
    }

    public String getExtraName() {
        return extraName;
    }

    public void setExtraName(String extraName) {
        this.extraName = extraName;
    }

    public String getExtraPrice() {
        return extraPrice;
    }

    public void setExtraPrice(String extraPrice) {
        this.extraPrice = extraPrice;
    }

    public int getSizeId() {
        return sizeId;
    }

    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }
}

