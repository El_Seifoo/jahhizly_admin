package com.example.seif.jahhizlyadmin2.Menu;

import java.io.Serializable;

/**
 * Created by seif on 1/7/2018.
 */
public class BrandCategories implements Serializable {
    private int id;
    private String name;

    public BrandCategories(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
