package com.example.seif.jahhizlyadmin2.Home;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Handler;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.LayoutDirection;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.seif.jahhizlyadmin2.Home.Orders.OrdersContainerFragment;
import com.example.seif.jahhizlyadmin2.Login.LoginActivity;
import com.example.seif.jahhizlyadmin2.Profile.ProfileActivity;
import com.example.seif.jahhizlyadmin2.R;
import com.example.seif.jahhizlyadmin2.Tables.TablesContainerFragment;
import com.example.seif.jahhizlyadmin2.Tables.TablesFragment;
import com.example.seif.jahhizlyadmin2.Utils.MySingleton;
import com.example.seif.jahhizlyadmin2.Utils.URLs;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import me.anwarshahriar.calligrapher.Calligrapher;

public class MainActivity extends AppCompatActivity {
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private Toolbar toolbar;
    FloatingActionButton fab;

    // index to know the current nav menu item
    public static int navItemIndex = 1;
    // Tag for attaching the fragments
    private static final String TAG_ORDERS = "orders";
    private static final String TAG_TABLES = "tables";
    // Tag for current attaching fragment
    public static String CURRENT_TAG = TAG_ORDERS;
    // toolbar titles based on selected nav menu item
    private String[] activityTitles;
    // flag to load makeOrder fragment when user presses back key
    private boolean shouldLoadMakeOrderFragOnBackPress = true;
    private Handler mHandler;
    private Runnable runnable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String localLanguage = MySingleton.getmInstance(this).getAppLang();
        if (localLanguage.equals(getString(R.string.settings_language_arabic_value))) {
            setLocale("ar", this);
        } else if (localLanguage.equals(getString(R.string.settings_language_english_value))) {
            Locale localeEn = new Locale("en");
            setLocale("en", this);
        }
        setContentView(R.layout.activity_main);
        Calligrapher calligrapher = new Calligrapher(this);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(this, "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(this, "fonts/English/Roboto_Regular.ttf", true);
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.app_name));
        mHandler = new Handler();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        // get toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);
        // load navigation header
        loadHeader();
        // initialize navigation
        setUpNavigation();

        //fab
        fab = (FloatingActionButton) findViewById(R.id.swap_fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (navItemIndex == 1) {
                    navItemIndex = 2;
                    CURRENT_TAG = TAG_TABLES;
                    loadOrdersFragment();
                } else if (navItemIndex == 2) {
                    navItemIndex = 1;
                    CURRENT_TAG = TAG_ORDERS;
                    loadOrdersFragment();
                }
            }
        });

        if (savedInstanceState == null) {
            navItemIndex = 1;
            CURRENT_TAG = TAG_ORDERS;
            loadOrdersFragment();
        }

    }

    public void loadHeader() {
        Glide.with(this).load(URLs.ROOT_URL + MySingleton.getmInstance(this).userData().getPhoto())
                .error(R.mipmap.restaurant_meal_icon)
                .crossFade()
                .thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(((CircleImageView) navHeader.findViewById(R.id.brand_logo)));
        ((TextView) navHeader.findViewById(R.id.user_name)).setText(MySingleton.getmInstance(this).userData().getName());
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            ((TextView) navHeader.findViewById(R.id.branch_name)).setText(MySingleton.getmInstance(this).userData().getBranchArabicName());
        } else {
            ((TextView) navHeader.findViewById(R.id.branch_name)).setText(MySingleton.getmInstance(this).userData().getBranchName());
        }
    }

    public void setUpNavigation() {
        // Handle navigation menu items actions
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                Log.e("seifooo", navItemIndex + "??" + CURRENT_TAG);
                switch (item.getItemId()) {
                    case R.id.nav_profile:
                        //start profile activity
                        startActivity(new Intent(MainActivity.this, ProfileActivity.class));
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_order:
                        //start orders fragment
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_ORDERS;
                        break;
                    case R.id.nav_table:
                        //start tables fragment
                        navItemIndex = 2;
                        CURRENT_TAG = TAG_TABLES;
                        break;
                    case R.id.nav_log_out:
                        MySingleton.getmInstance(MainActivity.this).logout();
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.putExtra("Logout", "Logout");
                        startActivity(intent);
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_lang:
                        if (MySingleton.getmInstance(MainActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                            MySingleton.getmInstance(MainActivity.this).setAppLang(getString(R.string.settings_language_english_value));
                        } else if (MySingleton.getmInstance(MainActivity.this).getAppLang().equals(getString(R.string.settings_language_english_value))) {
                            MySingleton.getmInstance(MainActivity.this).setAppLang(getString(R.string.settings_language_arabic_value));
                        }
                        finish();
                        startActivity(getIntent());
                        drawer.closeDrawers();
                        return true;
                    default:
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_ORDERS;
                        break;
                }

                loadOrdersFragment();

                return true;
            }
        });

        android.support.v7.app.ActionBarDrawerToggle actionBarDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                FrameLayout frame = (FrameLayout) findViewById(R.id.frame_container);
                if (getResources().getConfiguration().getLayoutDirection() == LayoutDirection.RTL) {
                    frame.setX(-slideOffset * drawerView.getWidth());
                } else {
                    frame.setX(slideOffset * drawerView.getWidth());
                }
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    /*
     *  return selected fragment from navigation menu
     */
    public void loadOrdersFragment() {
        if (getIntent().hasExtra("GoToTables") || (getIntent().hasExtra("extra") && getIntent().getExtras().getString("extra").equals("table"))) {
            navItemIndex = 2;
            CURRENT_TAG = TAG_TABLES;
            if (getIntent().hasExtra("GoToTables")) {
                getIntent().removeExtra("GoToTables");
            } else if (getIntent().hasExtra("extra")) {
                getIntent().removeExtra("extra");
            }
        }
        if (navItemIndex == 1) {
            fab.setImageResource(R.mipmap.sidemenu_booktable);
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) fab.getLayoutParams();
            params.setMargins(48, 0, 48, 250);
            fab.setLayoutParams(params);
        } else if (navItemIndex == 2) {
            fab.setImageResource(R.mipmap.sidemenu_orders);
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) fab.getLayoutParams();
            params.setMargins(48, 0, 48, 48);
            fab.setLayoutParams(params);
        }

        // selecting appropriate navigation menu item
        selectNavmenu();

        // set toolbar title
        setToolbarTitle();

        // close the navigation drawer if the user selected the current navigation menu item
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();
            return;
        }

        runnable = new Runnable() {
            @Override
            public void run() {
                Fragment fragment = getSelectedFragment();
                if (fragment != null) {
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                            android.R.anim.fade_out);
                    fragmentTransaction.replace(R.id.frame, fragment);
                    fragmentTransaction.commitAllowingStateLoss();
                }
            }
        };

        if (runnable != null) {
            mHandler.post(runnable);
        }

        // closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    public void selectNavmenu() {
        for (int i = 0; i < 6; i++) {
            if (i == navItemIndex) {
                navigationView.getMenu().getItem(i).setChecked(true);
            } else {
                navigationView.getMenu().getItem(i).setChecked(false);
            }
        }
    }

    public void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    public Fragment getSelectedFragment() {
        switch (navItemIndex) {
            case 1:
                return OrdersContainerFragment.newInstance(key);
            case 2:
                return TablesContainerFragment.newInstance(key);
            default:
                return OrdersContainerFragment.newInstance(key);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // load MakeOrderFragment if user pressed back in other fragment
        if (shouldLoadMakeOrderFragOnBackPress) {
            if (navItemIndex != 1) {
                navItemIndex = 1;
                CURRENT_TAG = TAG_ORDERS;
                loadOrdersFragment();
                return;
            }
        }
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // make items shows in OrdersFragment only
        if (navItemIndex == 1 || navItemIndex == 2) {
            getMenuInflater().inflate(R.menu.main_menu, menu);
            MenuItem item = menu.findItem(R.id.action_search);
            setSearchView();
            searchView.setMenuItem(item);
        }
        return true;
    }

    MaterialSearchView searchView;
    String key;

    private void setSearchView() {
        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
                                              @Override
                                              public boolean onQueryTextSubmit(String query) {
                                                  key = query;
                                                  Log.e("sssss", key + "||||||" + query);
                                                  loadOrdersFragment();
                                                  //Do some magic
                                                  return true;
                                              }


                                              @Override
                                              public boolean onQueryTextChange(String newText) {
                                                  key = newText;
                                                  loadOrdersFragment();
                                                  //Do some magic
                                                  return true;
                                              }
                                          }
        );

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener()

                                           {
                                               @Override
                                               public void onSearchViewShown() {
                                                   //Do some magic
                                               }

                                               @Override
                                               public void onSearchViewClosed() {
                                                   //Do some magic
                                               }
                                           }

        );
    }

    private void setLocale(String lang, Context context) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocale(locale);
            getApplicationContext().getResources().updateConfiguration(config, null);
        } else {
            config.locale = locale;
            getApplicationContext().getResources().updateConfiguration(config, null);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(locale);
            config.setLayoutDirection(locale);
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
            context.createConfigurationContext(config);
        } else {
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        }
    }



}
