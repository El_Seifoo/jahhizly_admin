package com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.seif.jahhizlyadmin2.Utils.MySingleton;
import com.example.seif.jahhizlyadmin2.Utils.URLs;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by seif on 1/4/2018.
 */
public class OrderModel {
    public OrderModel() {
    }

    protected void getOrderDetails(final Context context, final VolleyCallback callback, int orderId) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URLs.ORDER_DETAILS + "/" + orderId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            callback.onSuccess(context, response, 1, 0);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFail(context, error, 1);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void getSizesExtras(final Context context, final VolleyCallback callback, final int menuId, final int index) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URLs.MENU_ITEM_DETAILS + "/" + menuId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            callback.onSuccess(context, response, 2, index);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onFail(context, error, 2);
                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void editOrder(final Context context, final VolleyCallback callback, int orderId, int userId,
                             int branchId, int orderTime, ArrayList<OrdersDetails> orderList, int status) throws JSONException {
        JSONObject order = new JSONObject();
        order.put("id", orderId);
        order.put("user_id", userId);
        order.put("branch_id", branchId);
        order.put("order_time", convertDigits(orderTime + ""));
        order.put("status", status);
        JSONArray items = new JSONArray();
        for (int i = 0; i < orderList.size(); i++) {
            JSONObject data = new JSONObject();
            Log.e("ssssssss", orderList.get(i).getId() + "");
            if (String.valueOf(orderList.get(i).getId()) != null && orderList.get(i).getId() != 0) {
                data.put("id", orderList.get(i).getId());
            }
            data.put("menu_id", orderList.get(i).getMenuId());
            data.put("size_id", orderList.get(i).getSizeId());
            data.put("quantity", orderList.get(i).getQuantity());
            if (orderList.get(i).getExtraId() != null) {
                if (orderList.get(i).getExtraId().contains(",")) {
                    String[] extrasIds = orderList.get(i).getExtraId().split(",");
                    JSONArray extrasIdsArray = new JSONArray();
                    for (int x = 0; x < extrasIds.length; x++) {
                        extrasIdsArray.put(extrasIds[x]);
                    }
                    data.put("extras", extrasIdsArray);
                } else if (!orderList.get(i).getExtraId().isEmpty() && !orderList.get(i).getExtraId().contains(",")) {
                    JSONArray extrasIdsArray = new JSONArray();
                    extrasIdsArray.put(orderList.get(i).getExtraId());
                    data.put("extras", extrasIdsArray);
                }
            }
            items.put(i, data);
        }
        order.put("items", items);
        Log.e("Edited Order", order.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                URLs.EDIT_ORDER,
                order,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Edit Order response", response.toString());
                        try {
                            callback.onSuccess(context, response.toString(), 3, 0);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("Edit Order parsing", e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Edit Order error", error.toString());
                        callback.onFail(context, error, 3);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(jsonObjectRequest);

    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }


    protected void deleteOrder(final Context context, final VolleyCallback callback, int orderId) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URLs.DELETE_ORDER + "/" + orderId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Delete order response", response);
                        try {
                            callback.onSuccess(context, response, 4, 0);
                        } catch (JSONException e) {
                            Log.e("Delete parsing error", e.toString());
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Delete order error", error.toString());
                        callback.onFail(context, error, 4);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected interface VolleyCallback {
        void onSuccess(Context context, String response, int which, int index) throws JSONException;

        void onFail(Context context, VolleyError error, int which);
    }
}
