package com.example.seif.jahhizlyadmin2.Home.Orders;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.edmodo.rangebar.RangeBar;
import com.example.seif.jahhizlyadmin2.Home.HomeMVP;
import com.example.seif.jahhizlyadmin2.Home.HomeModel;
import com.example.seif.jahhizlyadmin2.Home.HomePresenter;

import com.example.seif.jahhizlyadmin2.Home.Orders.OrderDetails.OrderDetailsActivity;
import com.example.seif.jahhizlyadmin2.R;
import com.example.seif.jahhizlyadmin2.Utils.MySingleton;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

import static android.content.Context.VIBRATOR_SERVICE;

/**
 * Created by seif on 1/4/2018.
 */
public class OrdersFragment extends Fragment implements HomeMVP.OrdersView, OrdersAdapter.OrdersListItemClickListener {
    private FloatingActionButton fab;
    private HomePresenter presenter;
    private MKLoader loading, loadingMore;
    int page = 0;
    RecyclerView ordersRecyclerView;

    OrdersAdapter adapter;
    ArrayList<Orders> allOrders = new ArrayList<>();
    private Timer timer;

    public static OrdersFragment newInstance(String status, String key) {
        OrdersFragment fragment = new OrdersFragment();
        Bundle args = new Bundle();
        // args.put data
        args.putString("Status", (status != null) ? status : "all");
        args.putString("Key", (key != null) ? key : "");
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_orders, container, false);
        design(view);
        status = getArguments().getString("Status");
        key = getArguments().getString("Key");
        presenter = new HomePresenter(this, new HomeModel(),
                MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
        loading = (MKLoader) view.findViewById(R.id.loading);
        loadingMore = (MKLoader) view.findViewById(R.id.loading_more);
        emptyTxt = (TextView) view.findViewById(R.id.empty_list_text);
        ordersRecyclerView = (RecyclerView) view.findViewById(R.id.orders_recycler_view);
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                ordersRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false));
                adapter = new OrdersAdapter(this, metrics.widthPixels, metrics.heightPixels, 0, MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                ordersRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false));
                adapter = new OrdersAdapter(this, metrics.widthPixels, metrics.heightPixels, 1, MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                adapter = new OrdersAdapter(this, metrics.widthPixels, metrics.heightPixels, 2, MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
                ordersRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                break;
            default:
                adapter = new OrdersAdapter(this, metrics.widthPixels, metrics.heightPixels, 3, MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
                ordersRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        }
        ordersRecyclerView.setHasFixedSize(true);
        presenter.requestOrders(getContext(), page, status, minimumString != null ? convertDigits1(minimumString) : "all", maximumString != null ? convertDigits1(maximumString) : "all", convertDigits1(key));
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onFabClicked();
            }
        });
        ordersRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int pastVisiblesItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
                if (dy > 0 && pastVisiblesItems >= adapter.getItemCount() - 1) {
                    page++;
                    presenter.requestOrders(getContext(), page, status, minimumString != null ? convertDigits1(minimumString) : "all", maximumString != null ? convertDigits1(maximumString) : "all", convertDigits1(key));
                }
            }
        });


        return view;
    }

    private float getScreenWidth() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        float scaleFactor = displayMetrics.density;
        return (width / scaleFactor);
    }

    @Override
    public void onResume() {
        super.onResume();
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
                                      @Override
                                      public void run() {
                                          if (getContext() != null && page == 0) {
                                              Log.e("llllllll", "lllllllll");
                                              presenter.requestOrders(getContext(), status, minimumString != null ? convertDigits1(minimumString) : "all", maximumString != null ? convertDigits1(maximumString) : "all", convertDigits1(key));
                                          }
                                      }
                                  }

                , 10000, 50000);
    }

    private void design(View view) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(params);
    }

    String status;
    String minimumString;
    String maximumString;
    String key;

    @Override
    public void fabAction() {
        minimumString = "all";
        maximumString = "all";
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.orders_filter);
        if (MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            dialog.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
        final TextView max, min;
        max = (TextView) dialog.findViewById(R.id.maximum);
        min = (TextView) dialog.findViewById(R.id.minimum);
        RangeBar rangebar = (RangeBar) dialog.findViewById(R.id.rangebar);
        rangebar.setBarColor(Color.parseColor("#4c4c4c"));
        rangebar.setConnectingLineColor(Color.parseColor("#eccd33"));
        rangebar.setThumbColorNormal(Color.parseColor("#eccd33"));
        rangebar.setThumbColorPressed(Color.parseColor("#eccd33"));
        rangebar.setTickHeight(0);
        rangebar.setTickCount((int) (getMaxPrice(allOrders) + 1));
        min.setText(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits("0") : "0");
        max.setText(String.valueOf(getMaxPrice(allOrders) + 1));
        rangebar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int leftThumbIndex, int rightThumbIndex) {
                if (MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                    min.setText((leftThumbIndex <= 10) ? convertDigits("0") : convertDigits(String.valueOf(leftThumbIndex)));
                    max.setText((rightThumbIndex >= ((getMaxPrice(allOrders) + 1) - 10)) ? convertDigits(String.valueOf(getMaxPrice(allOrders) + 1)) : convertDigits(String.valueOf(rightThumbIndex)));
                } else {
                    min.setText((leftThumbIndex <= 10) ? convertDigits("0") : convertDigits(String.valueOf(leftThumbIndex)));
                    max.setText((rightThumbIndex >= ((getMaxPrice(allOrders) + 1) - 10)) ? convertDigits(String.valueOf(getMaxPrice(allOrders) + 1)) : convertDigits(String.valueOf(rightThumbIndex)));
                }

            }
        });

        ((TextView) dialog.findViewById(R.id.dialog_done_txt)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                maximumString = max.getText().toString().trim();
                minimumString = min.getText().toString().trim();
                presenter.requestOrders(getContext(), page, convertDigits1(status), convertDigits1(minimumString), convertDigits1(maximumString), convertDigits1(key));
                dialog.dismiss();
            }
        });

        ((TextView) dialog.findViewById(R.id.dialog_cancel_txt)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
    }

    private double getMaxPrice(ArrayList<Orders> list) {
        double max = list.get(0).getPrice();
        for (int i = 1; i < list.size(); i++) {
            if (max < list.get(i).getPrice()) {
                max = list.get(i).getPrice();
            }
        }
        return max;
    }

    @Override
    public void showProgress() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        loading.setVisibility(View.GONE);
    }

    @Override
    public void showLoadingMoreProgress() {
        emptyTxt.setVisibility(View.GONE);
        loadingMore.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingMoreProgress() {
        emptyTxt.setVisibility(View.GONE);
        loadingMore.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String message) {
        if (getContext() != null) {
            Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void loadData(ArrayList<Orders> orders) {
        if (getContext() != null) {
            if (emptyTxt.getVisibility() == View.VISIBLE) {
                emptyTxt.setVisibility(View.GONE);
            }
            if (MySingleton.getmInstance(getContext()).isLoggedIn()) {
                for (int i = 0; i < orders.size(); i++) {
                    orders.get(i).setPhoto(MySingleton.getmInstance(getContext()).userData().getBrandLogo());
                    if (MySingleton.getmInstance(getContext()).getAppLang().equals(getContext().getString(R.string.settings_language_arabic_value))) {
                        orders.get(i).setBranchName(MySingleton.getmInstance(getContext()).userData().getBranchArabicName());
                    } else {
                        orders.get(i).setBranchName(MySingleton.getmInstance(getContext()).userData().getBranchName());
                    }
                }
            }

            if (page == 0 && !areIdentical(orders, allOrders)) {
                allOrders = orders;
                adapter.setData(orders);
                int pastVisiblesItems = ((LinearLayoutManager) ordersRecyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
                ordersRecyclerView.scrollToPosition(pastVisiblesItems - 3);
                ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
                scaleInAnimationAdapter.setFirstOnly(false);
                ordersRecyclerView.setAdapter(scaleInAnimationAdapter);
                adapter.notifyDataSetChanged();
                fab.setVisibility(View.VISIBLE);
            } else if (page == 0 && areIdentical(orders, allOrders)) {
                // Do nothing
            } else if (page != 0) {
                for (int i = 0; i < orders.size(); i++) {
                    allOrders.add(orders.get(i));
                }
                adapter.setData(allOrders);
                int pastVisiblesItems = ((LinearLayoutManager) ordersRecyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
                ordersRecyclerView.scrollToPosition(pastVisiblesItems - 3);
                ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
                scaleInAnimationAdapter.setFirstOnly(false);
                ordersRecyclerView.setAdapter(scaleInAnimationAdapter);
                fab.setVisibility(View.VISIBLE);
            }

        }
    }

    private boolean areIdentical(ArrayList<Orders> orders1, ArrayList<Orders> orders2) {

        if (orders1 == null && orders2 == null)
            return true;
        if ((orders1 == null && orders2 != null) || (orders2 == null && orders1 != null))
            return false;

        if (orders1.size() != orders2.size())
            return false;


        for (int i = 0; i < orders1.size(); i++) {
            if (!(orders1.get(i).getId() == orders2.get(i).getId()))
                return false;
        }
        return true;
    }

    TextView emptyTxt;

    @Override
    public void showEmptyTxt() {
        emptyTxt.setVisibility(View.VISIBLE);
    }

    @Override
    public void onListItemClickListener(int position) {
        Intent intent = new Intent(getContext(), OrderDetailsActivity.class);
        intent.putExtra("UserOrder", allOrders.get(position));
        startActivity(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (timer != null) {
            timer.cancel();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (timer != null) {
            timer.cancel();
        }
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

    private String convertDigits1(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }


}
